import keras
from keras import backend as K
import os
import cv2
import numpy as np
import math
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
import coremltools

symbol_label_list = ["background", "solid_note_head"]
uni_size = (64, 64, 3)
uni_image_pixels = uni_size[0]*uni_size[1]

def train_cnn():
    batch_size = 50
    num_classes = len(symbol_label_list)
    epochs = 10

    img_rows, img_cols, img_channels = uni_size[0], uni_size[1], uni_size[2]

    print "Preparing data."

    x_train, y_train = get_data_batch(0, 3000)
    x_test, y_test = get_test_data_batch(0, 600)

    print "Training data ready."

    if K.image_data_format() == 'channels_first':
        x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
        x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
        input_shape = (1, img_rows, img_cols)
    else:
        # x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
        # x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
        input_shape = (img_rows, img_cols, 3)

    print('x_train shape:', x_train.shape)
    print(x_train.shape[0], 'train samples')
    print(x_test.shape[0], 'test samples')

    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)

    print 'Starting training.'

    model = Sequential()

    model.add(Conv2D(32, kernel_size=(5, 5),
                     activation='relu',
                     input_shape=input_shape,
                     padding='same'))
    model.add(MaxPooling2D(pool_size=(4, 4), padding='same'))

    model.add(Conv2D(64, (5, 5), activation='relu', padding='same'))
    model.add(MaxPooling2D(pool_size=(4, 4), padding='same'))

    model.add(Flatten())
    model.add(Dense(1024, activation='relu'))
    model.add(Dense(num_classes, activation='softmax'))

    model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=keras.optimizers.Adadelta(),
                  metrics=['accuracy'])

    model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epochs,
              verbose=1,
              validation_data=(x_test, y_test))

    score = model.evaluate(x_test, y_test, verbose=0)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])

    imgs = np.empty((1, uni_size[0], uni_size[1], uni_size[2]), dtype = 'float32')
    test_note_head_image = cv2.imread('/Users/brekhunchenko/Desktop/training_set/solid_note_head/2500.jpg', cv2.IMREAD_COLOR)
    # test_note_head_image = cv2.imread('/Users/brekhunchenko/Desktop/training_set/background/16.jpg', cv2.IMREAD_COLOR)
    test_note_head_image = unify_img_size(test_note_head_image)
    test_note_head_image = test_note_head_image/255.0
    imgs[0] = test_note_head_image

    (test_a, _) = get_data_batch(0, 20)
    test_a /= 255.0
    a = model.predict(test_a, 20, verbose=1)

    print 'Saving model.'

    coreml_model = coremltools.converters.keras.convert(model,
                                                        input_names=['input'],
                                                        output_names=['probs'],
                                                        image_input_names='input',
                                                        class_labels=['background', 'solid_note_head'],
                                                        predicted_feature_name='predicted_music_sheet_object',
                                                        image_scale=0.004)

    coreml_model_path = '/Users/brekhunchenko/Projects/recognize_music_pieces/models/keras/NoteHeadsRecognitionModel.mlmodel'
    coreml_model.save(coreml_model_path)

    print 'Model has beed saved at path %s' % coreml_model_path

def test_cnn():
    coreml_model_path = '/Users/brekhunchenko/Projects/recognize_music_pieces/models/keras/NoteHeadsRecognitionModel.mlmodel'
    restored_model = coremltools.models.MLModel(coreml_model_path)

    test_note_head_image = cv2.imread('/Users/brekhunchenko/Desktop/training_set/solid_note_head/3747.jpg', cv2.IMREAD_COLOR)
    test_note_head_image = unify_img_size(test_note_head_image)
    prediction = restored_model.predict({'input':test_note_head_image})

    print (prediction)

def unify_img_size(img):
    return cv2.resize(img, (uni_size[0], uni_size[1]), 0, 0, cv2.INTER_LINEAR)

def get_test_data_batch(i, batch_size):
    cnt = 0
    n_labels = len(symbol_label_list)
    imgs = np.empty((batch_size, uni_size[0], uni_size[1], uni_size[2]), dtype = 'float32')
    labels = np.zeros((batch_size), dtype='float32')
    circ = 0

    while cnt < batch_size:
        cur_label_index = circ % n_labels
        circ += 1

        label_name = symbol_label_list[cur_label_index]
        dir = "/Users/brekhunchenko/Desktop/test_set/" + label_name + "/"

        if not os.path.exists(dir):
            continue

        labels[cnt] = cur_label_index
        picked_index = cnt
        img = cv2.imread(dir + str(picked_index) + ".jpg", cv2.IMREAD_COLOR)
        imgs[cnt] = unify_img_size(img)/256.0
        cnt += 1

    batch = (imgs, labels)
    return batch

def get_data_batch(i, batch_size):
    cnt = 0
    n_labels = len(symbol_label_list)
    imgs = np.empty((batch_size, uni_size[0], uni_size[1], uni_size[2]), dtype = 'float32')
    labels = np.zeros((batch_size), dtype='float32')
    circ = 0

    while cnt < batch_size:
        cur_label_index = circ % n_labels
        circ += 1

        label_name = symbol_label_list[cur_label_index]
        dir = "/Users/brekhunchenko/Desktop/training_set/" + label_name + "/"

        if not os.path.exists(dir):
            continue

        labels[cnt] = cur_label_index
        picked_index = cnt
        img = cv2.imread(dir + str(picked_index) + ".jpg", cv2.IMREAD_COLOR)
        imgs[cnt] = unify_img_size(img)/256.0
        cnt += 1


    batch = (imgs, labels)
    return batch


train_cnn()
# test_cnn()