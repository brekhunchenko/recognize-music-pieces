//
//  ViewController.m
//  notes_training_set
//
//  Created by Yaroslav Brekhunchenko on 6/21/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () < UIScrollViewDelegate >

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) UIImage* image;
@property (nonatomic, strong) UIImageView* imageView;
@property (nonatomic, strong) UILongPressGestureRecognizer* longPressGesture;

@property (nonatomic, strong) NSMutableArray* notesPositions;
@property (nonatomic, strong) NSMutableArray* trainingImagesAreasViews;

@end

@implementation ViewController

#define kTrainingImageSize 1
#define kFileName @"IMG_6382.JPG"

#pragma mark - UIViewController Lifecycle

- (void)viewDidLoad {
  [super viewDidLoad];
  
  _scrollView.minimumZoomScale = 1.0f;
  _scrollView.maximumZoomScale = 4.0f;
  
  _image = [UIImage imageNamed:kFileName];
  _imageView = [[UIImageView alloc] initWithImage:_image];
  _imageView.frame = [UIScreen mainScreen].bounds;
  _imageView.contentMode = UIViewContentModeScaleAspectFit;
  _imageView.backgroundColor = [UIColor redColor];
  [_scrollView addSubview:_imageView];
  
  _longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGestureCatched:)];
  [_scrollView addGestureRecognizer:_longPressGesture];
  
  _notesPositions = [NSMutableArray new];
  _trainingImagesAreasViews = [NSMutableArray new];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
  return _imageView;
}

- (void)longPressGestureCatched:(UILongPressGestureRecognizer *)longPressGesture {
  if (longPressGesture.state == UIGestureRecognizerStateEnded) {
    CGPoint location = [longPressGesture locationInView:_scrollView];
    CGFloat scale = _scrollView.zoomScale;
    CGFloat imageScale = _scrollView.bounds.size.width/_image.size.width;
    
    location.x = location.x/imageScale/scale;
    location.y = location.y/imageScale/scale;
    
    UIView* rect = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, kTrainingImageSize, kTrainingImageSize)];
    rect.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:1.0f];
    rect.center = [longPressGesture locationInView:_imageView];
    [_imageView addSubview:rect];
    
    [_notesPositions addObject:[NSValue valueWithCGPoint:location]];
    [_trainingImagesAreasViews addObject:rect];
      
    NSLog(@"Note head position added: %@", NSStringFromCGPoint(location));
  }
}

- (IBAction)undoButtonAction:(id)sender {
    [_notesPositions removeLastObject];
    [[_trainingImagesAreasViews lastObject] removeFromSuperview];
    [_trainingImagesAreasViews removeLastObject];
    
    NSLog(@"Note head position has been removed. Last point: %@", NSStringFromCGPoint([_notesPositions.lastObject CGPointValue]));
}

- (IBAction)printButtonAction:(id)sender {
    printf("\n\n\n");
    printf("[");
    for (NSValue* point in _notesPositions) {
        CGPoint p = [point CGPointValue];
        printf("(%.1f, %.1f),", p.x, p.y);
    }
    printf("]");
    printf("\n\n\n");
}

- (IBAction)saveButtonAction:(id)sender {
    NSMutableDictionary* dict = [NSMutableDictionary new];
    NSMutableArray* points = [NSMutableArray new];
    for (NSValue* pointValue in _notesPositions) {
        CGPoint pt = [pointValue CGPointValue];
        int x = pt.x;
        int y= pt.y;
        [points addObject:@{@"x":@(x), @"y":@(y)}];
    }
    [dict setObject:points forKey:@"points"];
    
    NSError* err;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&err];
    NSString* myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString* jsonFilePath = [NSString stringWithFormat:@"/Users/brekhunchenko/Projects/recognize_music_pieces/photos_database/json_note_heads/%@.json", kFileName];
    [myString writeToFile:jsonFilePath
               atomically:YES
                 encoding:NSUTF8StringEncoding error:&err];
}

@end
