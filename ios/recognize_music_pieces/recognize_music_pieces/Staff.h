//
//  Staff.h
//  recognize_music_pieces
//
//  Created by Yaroslav Brekhunchenko on 6/19/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@interface Staff : NSObject

- (instancetype)initWithStaffWithLineThickness:(int)thickness spacing:(int)spacing peaks:(NSArray *)peaks tops:(NSArray *)tops;

@property (nonatomic, assign, readonly) int thickness;
@property (nonatomic, assign, readonly) int spacing;
@property (nonatomic, strong, readonly) NSArray* tops;
@property (nonatomic, strong, readonly) NSArray* peaks;

@end
