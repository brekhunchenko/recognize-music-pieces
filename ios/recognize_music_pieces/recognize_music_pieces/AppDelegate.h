//
//  AppDelegate.h
//  recognize_music_pieces
//
//  Created by Yaroslav Brekhunchenko on 6/17/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

