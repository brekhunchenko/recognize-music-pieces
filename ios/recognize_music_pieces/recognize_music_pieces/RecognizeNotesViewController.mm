//
//  RecognizeNotesViewController.m
//  recognize_music_pieces
//
//  Created by Yaroslav Brekhunchenko on 6/29/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import "RecognizeNotesViewController.h"
#import "MusicSheetsRecognizer.h"

#import "MBProgressHUD.h"
#import "UIImage+OpenCV.h"

#define TICK   NSDate *startTime = [NSDate date]
#define TOCK   NSLog(@"Time: %f", -[startTime timeIntervalSinceNow])

@interface RecognizeNotesViewController ()
 
@property (nonatomic, strong) UIView* containerView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@property (nonatomic, strong) MusicSheetsRecognizer* musicSheetsRecognizer;

@property (nonatomic, strong) MBProgressHUD* progressHUD;

@end

@implementation RecognizeNotesViewController

#pragma mark - UIViewController Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
  
    [self _setupMusicSheetsRecognitzer];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (_musicSheetImage) {
        [self _setupScrollViewForImages:@[_musicSheetImage]];
    }
}

#pragma mark - Setup

- (void)_setupMusicSheetsRecognitzer {
    _musicSheetsRecognizer = [[MusicSheetsRecognizer alloc] init];
}

- (void)_setupScrollViewForImages:(NSArray<UIImage *> *)images {
    [self.view layoutIfNeeded];
    
    if (_containerView == nil) {
        _containerView = [[UIView alloc] init];
    }
    _containerView.frame = CGRectMake(0.0f, 0.0f, _scrollView.bounds.size.width*images.count, _scrollView.bounds.size.height);
    [[_containerView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];

    for (int i = 0; i < images.count; i++) {
        UIImage* image = images[i];
        UIImageView* imageView = [[UIImageView alloc] init];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.image = image;
        imageView.backgroundColor = (i%2 == 0 ? [UIColor blueColor] : [UIColor redColor]);
        imageView.frame = CGRectMake(i*_scrollView.bounds.size.width, 0.0f, _scrollView.bounds.size.width, _scrollView.bounds.size.height);
        [_containerView addSubview:imageView];
    }
    _scrollView.minimumZoomScale = 1.0f;
    _scrollView.maximumZoomScale = 8.0f;
    _scrollView.scrollEnabled = YES;
    [_scrollView addSubview:_containerView];
}

#pragma mark - Utilz

- (void)_predictMusicSheetForImage:(cv::Mat)musicSheetOriginal {
    _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _progressHUD.mode = MBProgressHUDModeDeterminate;
    
    _musicSheetsRecognizer.dispatchQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    [_musicSheetsRecognizer predictMusicSheetFromImage:musicSheetOriginal progress:^(CGFloat progress) {
        NSLog(@"progress %f", progress);
        _progressHUD.progress = progress;
    } completionBlock:^(MusicSheetRecognitionResult *predictionResult) {
        _infoLabel.text = [NSString stringWithFormat:@"Predicted image: %@ \nNote heads: %lu \nDistance: %lu \nTime: %.1f", predictionResult.predictedImageName, predictionResult.noteHeadPositions.count, predictionResult.distance, predictionResult.time];
        [self _setupScrollViewForImages:predictionResult.imagesPipline];
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } error:^(NSError *error, MusicSheetRecognitionResult *predictionResult) {
        _infoLabel.text = [NSString stringWithFormat:@"Note heads: %lu \nDistance: %lu \nTime: %.1f", predictionResult.noteHeadPositions.count, predictionResult.distance, predictionResult.time];
        [self _setupScrollViewForImages:predictionResult.imagesPipline];
        
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:[NSString stringWithFormat:@"Wasn't able to recognize music sheet. Please try again."]  message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

- (void)_testRecognition {
    NSArray* listOfImageNames = @[
//                                  @"test_allegro.jpg",
                                  @"test_allegro_1.jpg",
                                  @"test_allegro_2_0.jpg",
                                  @"test_allegro_2_1.jpg",
                                  @"test_allegro_2_2.jpg",
                                  @"test_allegro_2_3.jpg",
                                  @"test_allegro_3.jpg",
                                  @"test_allegro_4.jpg",
                                  @"test_allgro_2.jpg",
                                  @"test_america.jpg",
                                  @"test_america_1.jpg",
                                  @"test_america_2.jpg",
                                  @"test_america_3.jpg",
                                  @"test_america_4.jpg",
                                  @"test_andante.jpg",
                                  @"test_andante_1.jpg",
                                  @"test_andante_2.jpg",
                                  @"test_andante_2_0.jpg",
                                  @"test_andante_2_1.jpg",
                                  @"test_andante_2_2.jpg",
                                  @"test_andante_2_3.jpg",
                                  @"test_andante_2_4.jpg",
                                  @"test_andante_4.jpg",
                                  @"test_andante_5.jpg",
                                  @"test_andante_6.jpg",
                                  @"test_bathwater_blues.jpg",
                                  @"test_bathwater_blues_1.jpg",
                                  @"test_bathwater_blues_2.jpg",
                                  @"test_bathwater_blues_2_0.jpg",
                                  @"test_bathwater_blues_2_1.jpg",
                                  @"test_bathwater_blues_2_2.jpg",
                                  @"test_bathwater_blues_3.jpg",
                                  @"test_dream.jpg",
                                  @"test_dream_1.jpg",
                                  @"test_dream_2.jpg",
                                  @"test_dream_3.jpg",
                                  @"test_dream_4.jpg",
                                  @"test_ground_after_the_scotch_humour.jpg",
                                  @"test_ground_after_the_scotch_humour_1.jpg",
                                  @"test_ground_after_the_scotch_humour_2.jpg",
                                  @"test_ground_after_the_scotch_humour_2_0.jpg",
                                  @"test_ground_after_the_scotch_humour_2_1.jpg",
                                  @"test_ground_after_the_scotch_humour_2_2.jpg",
                                  @"test_ground_after_the_scotch_humour_3.jpg",
                                  @"test_intermezzo.jpg",
                                  @"test_intermezzo_1.jpg",
                                  @"test_intermezzo_2_0.jpg",
                                  @"test_intermezzo_2_1.jpg",
                                  @"test_intermezzo_2_2.jpg",
                                  @"test_over_the_rainbow.jpg",
                                  @"test_over_the_rainbow_1.jpg",
                                  @"test_over_the_rainbow_2.jpg",
                                  @"test_patime_with_good_company.jpg",
                                  @"test_patime_with_good_company_1.jpg",
                                  @"test_patime_with_good_company_2.jpg",
                                  @"test_patime_with_good_company_3.jpg",
                                  @"test_pig_ankle_bag.jpg",
                                  @"test_pig_ankle_bag_1.jpg",
                                  @"test_pig_ankle_bag_2.jpg",
                                  @"test_pig_ankle_bag_3.jpg",
                                  @"test_polonaise.jpg",
                                  @"test_polonaise_1.jpg",
                                  @"test_polonaise_2.jpg",
                                  @"test_polonaise_3.jpg",
                                  @"test_presto.jpg",
                                  @"test_presto_1.jpg",
                                  @"test_romance.jpg",
                                  @"test_romance_1.jpg",
                                  @"test_romance_2.jpg",
                                  @"test_romance_2_0.jpg",
                                  @"test_romance_2_1.jpg",
                                  @"test_romance_2_2.jpg",
                                  @"test_romance_2_3.jpg",
                                  @"test_romance_3.jpg",
                                  @"test_rondino.jpg",
                                  @"test_rondino_1.jpg",
                                  @"test_rondino_2.jpg",
                                  @"test_rondino_2_0.jpg",
                                  @"test_rondino_2_1.jpg",
                                  @"test_rondino_2_2.jpg",
                                  @"test_rondino_2_3.jpg",
                                  @"test_rondino_3.jpg",
                                  @"test_the_folk_from_the_mountain.jpg",
                                  @"test_the_folk_from_the_mountain_1.jpg",
                                  @"test_the_folk_from_the_mountain_2.jpg",
                                  @"test_the_folk_from_the_mountain_2_0.jpg",
                                  @"test_the_folk_from_the_mountain_2_1.jpg",
                                  @"test_the_folk_from_the_mountain_2_3.jpg",
                                  @"test_the_folk_from_the_mountain_2_4.jpg",
                                  @"test_the_folk_from_the_mountain_2_5.jpg",
                                  @"test_the_folk_from_the_mountain_8.jpg",
                                  @"test_the_holy_boy.jpg",
                                  @"test_the_holy_boy_1.jpg",
                                  @"test_the_holy_boy_2.jpg",
                                  @"test_the_holy_boy_2_0.jpg",
                                  @"test_the_holy_boy_2_1.jpg",
                                  @"test_the_holy_boy_2_2.jpg",
                                  @"test_the_holy_boy_2_3.jpg",
                                  @"test_the_holy_boy_3.jpg",
                                  @"test_the_lark_in_the_clear_air.jpg",
                                  @"test_the_lark_in_the_clear_air_1.jpg",
                                  @"test_the_lark_in_the_clear_air_2.jpg",
                                  @"test_theme_and_variation.jpg",
                                  @"test_theme_and_variation_1.jpg",
                                  @"test_theme_and_variation_2.jpg",
                                  ];
    
    for (int i = 0; i < listOfImageNames.count; i++) {
        NSString* imageName = listOfImageNames[i];
        NSString *imagePath = [[NSBundle mainBundle] pathForResource:imageName ofType:@""];
        //      UIImage* image = [UIImage imageNamed:imageName];
        cv::Mat musicSheetOriginal = cv::imread(imagePath.fileSystemRepresentation, cv::IMREAD_GRAYSCALE);
        //      cv::Mat musicSheetOriginal = [UIImage cvMatFromUIImage:image];
        //      cv::cvtColor(musicSheetOriginal, musicSheetOriginal, CV_BGR2GRAY);
        cv::resize(musicSheetOriginal, musicSheetOriginal, cv::Size(1512, 2016));
        
        _musicSheetsRecognizer.dispatchQueue = dispatch_get_main_queue();
        
        [_musicSheetsRecognizer predictMusicSheetFromImage:musicSheetOriginal progress:nil completionBlock:^(MusicSheetRecognitionResult *predictionResult) {
            NSLog(@"%@",  [NSString stringWithFormat:@"%@ | %@ Time: %.1f", imageName, predictionResult.predictedImageName, predictionResult.time]);
        } error:^(NSError *error, MusicSheetRecognitionResult *predictionResult) {
            NSLog(@"Error: %@", imageName);
        }];
        break;
    }
}

#pragma mark - UIScrollView Delegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return _containerView;
}

#pragma mark - Actions

- (IBAction)calculateButtonAction:(id)sender {
//  self.musicSheetImage = [UIImage imageNamed:@"test_allegro_1.jpg"];
  cv::Mat musicSheetOriginal = [UIImage cvMatFromUIImage:self.musicSheetImage];
  cv::resize(musicSheetOriginal, musicSheetOriginal, cv::Size(1512, 2016));
  cv::cvtColor(musicSheetOriginal, musicSheetOriginal, CV_BGR2GRAY);
  [self _predictMusicSheetForImage:musicSheetOriginal];
    
//    [self _testRecognition];
}

- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
 
