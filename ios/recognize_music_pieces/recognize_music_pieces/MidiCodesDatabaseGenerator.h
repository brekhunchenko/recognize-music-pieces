//
//  MidiCodesDatabaseGenerator.h
//  recognize_music_pieces
//
//  Created by Yaroslav Brekhunchenko on 10/20/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <opencv2/core/core.hpp>
#import <opencv2/opencv.hpp>
#import <opencv2/imgproc/imgproc_c.h>
#import <opencv2/ml/ml.hpp>

@protocol MidiCodeFromMusicSheetImageOperationDelegate;

@interface MidiCodesDatabaseGenerator : NSObject

- (void)createDefaultMusicCodesDatabase;
- (void)createMusicCodesDatabaseForListOfImages:(NSArray *)listOfMusicSheets savePath:(NSString *)saveJSONPath;

@end

@interface MidiCodeFromMusicSheetImageOperation : NSOperation

- (instancetype)initWithMusicSheetImageName:(NSString *)imageName;
@property (nonatomic, strong, readonly) NSString* imageName;
@property (nonatomic, assign, readonly) cv::Mat musicSheetOriginal;

@property (nonatomic, weak) id<MidiCodeFromMusicSheetImageOperationDelegate> delegate;

@end

@protocol MidiCodeFromMusicSheetImageOperationDelegate

- (void)midiCodesFromMusicSheetImageOperatinoDidFinish:(MidiCodeFromMusicSheetImageOperation *)operation midiCode:(NSString *)midiCodes;

@end
