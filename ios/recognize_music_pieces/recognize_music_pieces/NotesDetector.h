//
//  NotesDetector.h
//  recognize_music_pieces
//
//  Created by Yaroslav Brekhunchenko on 6/29/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <opencv2/core/core.hpp>
#import <opencv2/opencv.hpp>
#import <opencv2/imgproc/imgproc_c.h>
#import <opencv2/ml/ml.hpp>

@interface NotesDetector : NSObject

- (void)detectNotesFromPossibleNotesImages:(std::vector<cv::Mat>)possibleImages
                                 positions:(std::vector<cv::Point>)positions
                                  progress:(void (^)(UIImage* progressImage, CGFloat progress))progressBlock
                                completion:(void (^)(std::vector<cv::Point> noteHeadPositions))completionBlock;

@end
