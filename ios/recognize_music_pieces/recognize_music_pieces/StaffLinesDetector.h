//
//  StaffLinesDetector.h
//  recognize_music_pieces
//
//  Created by Yaroslav Brekhunchenko on 6/19/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#include <opencv2/core/core.hpp>
#import <opencv2/opencv.hpp>
#import <opencv2/imgproc/imgproc_c.h>
#import <opencv2/ml/ml.hpp>

#import "Staff.h"

@interface StaffLinesDetector : NSObject

- (Staff *)detectStaffLinesFromImage:(cv::Mat)image;
- (Staff *)detectStaffLinesFromImage:(cv::Mat)image meanThreshold:(float)meanThreshold;
- (cv::Mat)cropStaffLinesFromImage:(cv::Mat)image staffTop:(NSNumber *)staffTop thickness:(int)thickness spacing:(int)spacing;
- (NSArray *)possibleRegionsForNoteHeadsFromStaffImage:(cv::Mat)staffBarImage staff:(Staff *)staff;

@end
