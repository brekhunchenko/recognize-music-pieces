//
//  CameraViewController.m
//  recognize_music_pieces
//
//  Created by Yaroslav Brekhunchenko on 6/29/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import "CameraViewController.h"
#import <opencv2/highgui/cap_ios.h>
#import "StaffLinesDetector.h"
#import "RecognizeNotesViewController.h"
#import "UIImage+Utilz.h"

@interface CameraViewController () < CvVideoCameraDelegate, AVCapturePhotoCaptureDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate >

@property (nonatomic, strong) CvVideoCamera *camera;
@property (nonatomic, strong) AVCapturePhotoOutput* photoOutput;
@property (nonatomic, strong) StaffLinesDetector* detector;

@property (weak, nonatomic) IBOutlet UIView *cameraOverlayView;

@end

@implementation CameraViewController

#pragma mark - UIViewController Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self _setupCamera];
    [self _setupLinesDetector];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [_camera start];
    
    _photoOutput = [[AVCapturePhotoOutput alloc] init];
    _photoOutput.highResolutionCaptureEnabled = YES;
    if ([_camera.captureSession canAddOutput:_photoOutput]) {
        [_camera.captureSession addOutput:_photoOutput];
    }
}

#pragma mark - Setup

- (void)_setupCamera {
    _camera = [[CvVideoCamera alloc] initWithParentView:_cameraOverlayView];
    _camera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionBack;
    _camera.defaultAVCaptureSessionPreset = AVCaptureSessionPreset640x480;
    _camera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
    _camera.defaultFPS = 30;
    _camera.grayscaleMode = NO;
    _camera.delegate = self;
    
//    AVCaptureDevice *flashLight = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
//    if ([flashLight isTorchAvailable] && [flashLight isTorchModeSupported:AVCaptureTorchModeOn]) {
//        BOOL success = [flashLight lockForConfiguration:nil];
//        if (success) {
////            [flashLight setTorchMode:AVCaptureTorchModeOn];
//            [flashLight unlockForConfiguration];
//        }
//    }
}

- (void)_setupLinesDetector {
    _detector = [[StaffLinesDetector alloc] init];
}

#pragma mark - Actions

- (IBAction)takePhotoButtonAction:(id)sender {
    AVCapturePhotoSettings* photoSettings = [[AVCapturePhotoSettings alloc] init];
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    BOOL hasFlash = [device hasFlash];
    if (hasFlash) {
        photoSettings.flashMode = AVCaptureFlashModeAuto;
    }
    photoSettings.autoStillImageStabilizationEnabled = YES;
    photoSettings.highResolutionPhotoEnabled = YES;
    [_photoOutput capturePhotoWithSettings:photoSettings delegate:self];
}

- (IBAction)chooseFromLibraryButtonAction:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)toggleFLash:(id)sender {
    AVCaptureDevice *flashLight = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if ([flashLight isTorchAvailable] && [flashLight isTorchModeSupported:AVCaptureTorchModeOn]) {
        BOOL success = [flashLight lockForConfiguration:nil];
        if (success) {
            if (flashLight.torchMode != AVCaptureTorchModeOn) {
                [flashLight setTorchMode:AVCaptureTorchModeOn];
                [flashLight unlockForConfiguration];
            } else {
                [flashLight setTorchMode:AVCaptureTorchModeOff];
                [flashLight unlockForConfiguration];
            }
        }
    }
}

#pragma mark - CvVideoCameraDelegate

- (void)processImage:(cv::Mat &)image {
//    cv::Mat proccessing = image.clone();
//    cvtColor(proccessing, proccessing, CV_BGRA2GRAY);
//
//    Staff* staff = [_detector detectStaffLinesFromImage:proccessing];
//    for (NSNumber* top in staff.peaks) {
//        cv::line(image,
//                 cv::Point(0.0f, top.floatValue),
//                 cv::Point(image.cols, top.floatValue),
//                 cv::Scalar(255,0,0), 1);
//    }
}

#pragma mark - AVCapturePhotoCaptureDelegate

- (void)captureOutput:(AVCapturePhotoOutput *)captureOutput didFinishProcessingPhotoSampleBuffer:(nullable CMSampleBufferRef)photoSampleBuffer previewPhotoSampleBuffer:(nullable CMSampleBufferRef)previewPhotoSampleBuffer
     resolvedSettings:(AVCaptureResolvedPhotoSettings *)resolvedSettings
      bracketSettings:(nullable AVCaptureBracketedStillImageSettings *)bracketSettings
                error:(nullable NSError *)error {
    NSData* photoData = [AVCapturePhotoOutput JPEGPhotoDataRepresentationForJPEGSampleBuffer:photoSampleBuffer
                                                                    previewPhotoSampleBuffer:previewPhotoSampleBuffer];
    UIImage* image = [UIImage imageWithData:photoData];
    image = [UIImage imageWithImage:image scaledToWidth:1512];
  
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    
    [_camera stop];
    
    RecognizeNotesViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([RecognizeNotesViewController class])];
    vc.musicSheetImage = image;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UIImagePickerController

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage* image = info[UIImagePickerControllerOriginalImage];
            image = [UIImage imageWithImage:image scaledToWidth:1512];
          
            [_camera stop];
            
            RecognizeNotesViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([RecognizeNotesViewController class])];
            vc.musicSheetImage = image;
            [self.navigationController pushViewController:vc animated:YES];
        });
    }];
}

@end
