//
//  RecognizeNotesViewController.h
//  recognize_music_pieces
//
//  Created by Yaroslav Brekhunchenko on 6/29/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecognizeNotesViewController : UIViewController

@property (nonatomic, strong) UIImage* musicSheetImage;

@end
