//
//  Staff.m
//  recognize_music_pieces
//
//  Created by Yaroslav Brekhunchenko on 6/19/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import "Staff.h"

@interface Staff()

@end

@implementation Staff

- (instancetype)initWithStaffWithLineThickness:(int)thickness spacing:(int)spacing peaks:(NSArray *)peaks tops:(NSArray *)tops {
    self = [super init];
    if (self) {
        _thickness = thickness;
        _spacing = spacing;
        _tops = tops;
        _peaks = peaks;
    }
    return self;
}

@end
