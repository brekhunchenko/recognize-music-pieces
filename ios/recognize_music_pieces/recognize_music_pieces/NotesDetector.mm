//
//  NotesDetector.m
//  recognize_music_pieces
//
//  Created by Yaroslav Brekhunchenko on 6/29/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import "NotesDetector.h"
#include <fstream>
#include <pthread.h>
#include <unistd.h>
#include <queue>
#include <sstream>
#include <string>

#import "NoteClassificationOperation.h"
#import "UIImage+OpenCV.h"

@interface NotesDetector() < NoteClassificationOperationDelegate >

@property (nonatomic, strong) NSOperationQueue* recognitionOperationsQueue;
@property (nonatomic, assign) NSInteger numberOfImagesForClassification;
@property (nonatomic, assign) NSInteger finishedNumberOfClassifiedImage;

@property (nonatomic, copy) void (^progressBlock)(UIImage* progressImage, CGFloat progress);

@end

@implementation NotesDetector {
    std::vector<cv::Point> notePositions;
}

#pragma mark - Initializers

- (instancetype)init {
    self = [super init];
    if (self) {
        _finishedNumberOfClassifiedImage = 0;
    }
    return self;
}

#pragma mark - Public Methods

- (void)detectNotesFromPossibleNotesImages:(std::vector<cv::Mat>)possibleImages
                                 positions:(std::vector<cv::Point>)possibleNotePositions
                                  progress:(void (^)(UIImage* progressImage, CGFloat progress))progressBlock
                                completion:(void (^)(std::vector<cv::Point> noteHeadPositions))completionBlock {
    assert(possibleImages.size() == possibleNotePositions.size());
    
    if (progressBlock) {
        _progressBlock = progressBlock;
    }
    
    _recognitionOperationsQueue = [[NSOperationQueue alloc] init];
    _recognitionOperationsQueue.maxConcurrentOperationCount = 1;
    NSMutableArray<__kindof NSOperation *>* operationsMutable = [NSMutableArray new];
    
    notePositions.clear();
    
    _numberOfImagesForClassification = possibleNotePositions.size();
    
    for (int k = 0; k < possibleImages.size(); k++) {
        int height = 64;
        int width = 64;
        cv::Mat readImage = possibleImages[k];
        cv::Size s(height,width);
        cv::Mat image = readImage;
        cv::resize(readImage, image, s, 0, 0, cv::INTER_LINEAR);
        
        UIImage* possibleNoteHeadImage = [UIImage imageWithCVMat:image];
        CGPoint possibleNoteHeadPosition = CGPointMake(possibleNotePositions[k].x, possibleNotePositions[k].y);
        NoteClassificationOperation* noteRecognitinoRequest = [[NoteClassificationOperation alloc] initWithImage:possibleNoteHeadImage notePosition:possibleNoteHeadPosition];
        noteRecognitinoRequest.delegate = self;
        NoteClassificationOperation* previousOperation = [operationsMutable lastObject];
        if (previousOperation) {
            [noteRecognitinoRequest addDependency:previousOperation];
        }
        [operationsMutable addObject:noteRecognitinoRequest];
    }
    
    if (operationsMutable.count > 0) {
        NSBlockOperation* finishedCallbackOperation = [NSBlockOperation blockOperationWithBlock:^{
            if (completionBlock) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completionBlock(notePositions);
                });
            }
        }];
        NoteClassificationOperation* lastOperation = [operationsMutable lastObject];
        [finishedCallbackOperation addDependency:lastOperation];
        [operationsMutable addObject:finishedCallbackOperation];
        
        [_recognitionOperationsQueue addOperations:operationsMutable waitUntilFinished:NO];
    }
}

#pragma mark - NoteRecognitionRequestDelegate

- (void)noteClassificationOperationDidFinish:(NoteClassificationOperation *)request isNote:(BOOL)isNote {
    _finishedNumberOfClassifiedImage++;
    
    if (_progressBlock) {
        dispatch_async(dispatch_get_main_queue(), ^{
            _progressBlock(nil, _finishedNumberOfClassifiedImage/(float)_numberOfImagesForClassification);
        });
    }
    
    if (isNote) {
        notePositions.push_back(cv::Point(request.notePosition.x, request.notePosition.y));
    }
}

@end
