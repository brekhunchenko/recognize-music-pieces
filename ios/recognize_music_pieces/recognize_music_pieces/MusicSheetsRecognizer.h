//
//  MusicSheetsRecognizer.h
//  recognize_music_pieces
//
//  Created by Yaroslav Brekhunchenko on 8/5/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <opencv2/core/core.hpp>
#import <opencv2/opencv.hpp>
#import <opencv2/imgproc/imgproc_c.h>
#import <opencv2/ml/ml.hpp>

#import <UIKit/UIKit.h>

#define DEBUG_MUSIC_SHEETS_RECOGNITION 

@class MusicSheetRecognitionResult, NoteHead;

@interface MusicSheetsRecognizer : NSObject

@property (nonatomic, strong) dispatch_queue_t dispatchQueue;

- (void)predictMusicSheetFromImage:(cv::Mat)musicSheetOriginal
                           progress:(void (^)(CGFloat progress))progressBlock
                    completionBlock:(void (^)(MusicSheetRecognitionResult* result))completionBlock
                              error:(void (^)(NSError *error, MusicSheetRecognitionResult* result))error;

- (void)midiCodeStringFromImage:(cv::Mat)musicSheetOriginal progress:(void (^)(CGFloat progress))progressBlock completion:(void (^)(NSString *))completionBlock;

@end

@interface MusicSheetRecognitionResult : NSObject

@property (nonatomic, strong) NSString* predictedImageName;
@property (nonatomic, strong) NSArray<NoteHead *> *noteHeadPositions;
@property (nonatomic, assign) NSInteger distance;
@property (nonatomic, strong) NSArray<UIImage *>* imagesPipline;
@property (nonatomic, assign) NSTimeInterval time;

@end
