//
//  MusicSheetsRecognizer.m
//  recognize_music_pieces
//
//  Created by Yaroslav Brekhunchenko on 8/5/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import "MusicSheetsRecognizer.h"
#import "StaffLinesDetector.h"
#import "Staff.h"
#import "NotesDetector.h"
#import "NoteHead.h"
#import "UIImage+OpenCV.h"

#define TICK   NSDate *startTime = [NSDate date]
#define TOCK   NSLog(@"Time: %f", -[startTime timeIntervalSinceNow])

@interface MusicSheetsRecognizer()

@property (nonatomic, strong) StaffLinesDetector* linesDetector;

/* DEBUG */
@property (nonatomic, strong) NSMutableArray<UIImage *> *imagesPipline;
@property (nonatomic, strong) NSArray<NoteHead *> *noteHeads;

@end

@implementation MusicSheetsRecognizer

#pragma mark - Initializers

- (instancetype)init {
  self = [super init];
  if (self) {
        _dispatchQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
      
        _linesDetector = [[StaffLinesDetector alloc] init];

        _imagesPipline = [NSMutableArray new];
  }
  return self;
}

#pragma mark - Public Methods

- (void)midiCodeStringFromImage:(cv::Mat)musicSheetOriginal progress:(void (^)(CGFloat progress))progressBlock completion:(void (^)(NSString *))completionBlock {
    dispatch_async(_dispatchQueue, ^{
        Staff* originalImageStaffInfo = [_linesDetector detectStaffLinesFromImage:musicSheetOriginal.clone()];
        //NSLog(@"Staff bar thickness: %d. Line spacing: %d. Founded peaks: %lu. Tops: %lu", staff.thickness, staff.spacing, staff.peaks.count, staff.tops.count);

        if (progressBlock) progressBlock(0.1f);

        cv::Mat proccessing = musicSheetOriginal.clone();
        std::vector<cv::KeyPoint> keypoints;
        
        int minMeanThreshold =  2;
        int maxMeanThreshold = 4;
        int bestMeanThreshold = minMeanThreshold;
        for (int meanThreshold = minMeanThreshold; meanThreshold < maxMeanThreshold; meanThreshold++) {
            cv::Mat matchHighlightImage = proccessing.clone();
            cv::adaptiveThreshold(matchHighlightImage, matchHighlightImage, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C, CV_THRESH_BINARY, 99, 10);
            
            cv::Mat kernel = cv::Mat::ones(originalImageStaffInfo.thickness+1, originalImageStaffInfo.thickness+1, CV_32F);
            cv::dilate(matchHighlightImage, matchHighlightImage, kernel, cv::Point(-1, -1), meanThreshold);
            cv::erode(matchHighlightImage, matchHighlightImage, kernel, cv::Point(-1, -1), 2);
            
            std::vector<cv::KeyPoint> detectedKeypoints = [self _detectPossibleNoteHeadsPositions:matchHighlightImage staff:originalImageStaffInfo originial:musicSheetOriginal];
            if (detectedKeypoints.size() > keypoints.size()) {
                #ifdef DEBUG_MUSIC_SHEETS_RECOGNITION
                [_imagesPipline addObject:[UIImage imageWithCVMat:matchHighlightImage]];
                cv::imwrite("/Users/brekhunchenko/Desktop/recognition_pipline_ios/binary_before_getting_circles.png", matchHighlightImage);
                #endif

                keypoints = detectedKeypoints;
                bestMeanThreshold = meanThreshold;
            }
        }
        
        __block std::vector<cv::Mat> possibleNoteImages;
        __block std::vector<cv::Point> possibleNotePositions;
        for (int i = 0; i < keypoints.size(); i++) {
            cv::KeyPoint p = keypoints[i];
            
            /* Get line spacing and line thickness info for the note head region */
            int size = originalImageStaffInfo.spacing*13;
            int subImageXStart = p.pt.x - size/2.0f > 0 ? p.pt.x - size/2.0f : 0.0f;
            int subImageYStart = p.pt.y - size/2.0f > 0 ? p.pt.y - size/2.0f : 0.0f;
            int subImageXEnd = p.pt.x + size/2.0f < musicSheetOriginal.cols ? p.pt.x + size/2.0f : musicSheetOriginal.cols;
            int subImageYEnd = p.pt.y + size/2.0f < musicSheetOriginal.rows ? p.pt.y + size/2.0f : musicSheetOriginal.rows;
            cv::Rect rect(subImageXStart, subImageYStart, subImageXEnd - subImageXStart, subImageYEnd - subImageYStart);
            cv::Mat subImg(musicSheetOriginal, rect);
            Staff* subImageStaff = [_linesDetector detectStaffLinesFromImage:subImg.clone() meanThreshold:3.0f];
            
            int xStart = p.pt.x - subImageStaff.spacing*1.25;
            int yStart = p.pt.y - subImageStaff.spacing*1.25;
            int width = subImageStaff.spacing*2.5;
            int height = subImageStaff.spacing*2.5;
            cv::Rect subImgRect(xStart, yStart, width, height);
            if (subImgRect.tl().x > 0 && subImgRect.tl().y > 0 && subImgRect.br().x < musicSheetOriginal.cols && subImgRect.br().y < musicSheetOriginal.rows && subImgRect.width > 0 && subImgRect.height > 0) {
                cv::Mat updatedSubImg(musicSheetOriginal, subImgRect);
                possibleNoteImages.push_back(updatedSubImg);
                possibleNotePositions.push_back(cv::Point(p.pt.x, p.pt.y));
            }
        }

        if (progressBlock) progressBlock(0.25f);

    #ifdef DEBUG_MUSIC_SHEETS_RECOGNITION
        for (int i = 0; i < possibleNoteImages.size(); i++) {
            cv::Mat sub_im = possibleNoteImages[i];
            std::ostringstream stringStream;
            stringStream << "/Users/brekhunchenko/Desktop/recognition_pipline_ios/notes/" << i << ".png";
            std::string path = stringStream.str();
            cv::imwrite(path, sub_im);
        }
    #endif
        
        __weak typeof(self) weakSelf = self;
        NotesDetector* notesDetector = [[NotesDetector alloc] init];
        [notesDetector detectNotesFromPossibleNotesImages:possibleNoteImages positions:possibleNotePositions progress:^(UIImage* progressImage, CGFloat progress) {
            if (progressBlock) {
                progressBlock(0.2f + progress*0.4f);
            }
        } completion:^(std::vector<cv::Point> notePositions) {
            NSArray<NoteHead *> *noteHeads = [weakSelf _getNoteHeadsFromNoteHeadPositions:notePositions staff:originalImageStaffInfo image:musicSheetOriginal progress:^(CGFloat progress) {
                if (progressBlock) {
                    progressBlock(0.6f + progress*0.35f);
                }
            }];
            
    #ifdef DEBUG_MUSIC_SHEETS_RECOGNITION
            weakSelf.noteHeads = noteHeads;
            
            cv::Mat showNoteHeadPositionsImage = musicSheetOriginal.clone();
            cv::cvtColor(showNoteHeadPositionsImage, showNoteHeadPositionsImage, CV_GRAY2BGR);
            for (int i = 0; i < noteHeads.count; i++) {
                NoteHead* noteHead = noteHeads[i];
                cv::Point pt = cv::Point(noteHead.noteHeadPosition.x, noteHead.noteHeadPosition.y);
                cv::Rect rect(pt.x - originalImageStaffInfo.spacing, pt.y - originalImageStaffInfo.spacing, originalImageStaffInfo.spacing*2, originalImageStaffInfo.spacing*2);
                cv::rectangle(showNoteHeadPositionsImage, rect, cv::Scalar(0,255,0));
                cv::putText(showNoteHeadPositionsImage, std::to_string(i), cv::Point((int)pt.x, (int)noteHead.topClosestConverted), 2, 0.5, cv::Scalar(255,0,0));
                cv::line(showNoteHeadPositionsImage, pt, cv::Point((int)pt.x, (int)noteHead.topClosestConverted), cv::Scalar(0, 255, 0));
            }
            
            [weakSelf.imagesPipline insertObject:[UIImage imageWithCVMat:showNoteHeadPositionsImage] atIndex:0];
            cv::imwrite("/Users/brekhunchenko/Desktop/recognition_pipline_ios/note_heads.png", showNoteHeadPositionsImage);
            //NSLog(@"note_heads.png has been created.");
    #endif
            
            NSString* imageMidiCodes = [weakSelf _getMidiCodesFromNoteHeads:noteHeads staff:originalImageStaffInfo];
            if (completionBlock) {
                completionBlock(imageMidiCodes);
            }
        }];
    });
}

- (void )predictMusicSheetFromImage:(cv::Mat)musicSheetOriginal progress:(void (^)(CGFloat progress))progressBlock
                    completionBlock:(void (^)(MusicSheetRecognitionResult* result))completionBlock
                              error:(void (^)(NSError *error, MusicSheetRecognitionResult* result))errorBlock {
    assert(musicSheetOriginal.cols > 0);
    assert(musicSheetOriginal.rows > 0);
    
    NSDate* startTime = [NSDate date];
    [_imagesPipline removeAllObjects];
    [self midiCodeStringFromImage:musicSheetOriginal progress:^(CGFloat progress) {
        if ([NSThread isMainThread]) {
            if (progressBlock) {
                progressBlock(progress);
            }
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (progressBlock) {
                    progressBlock(progress);
                }
            });
        }
    } completion:^(NSString *imageMidiCodes) {
        size_t min_dist = INTMAX_MAX;
        NSString* predictedName = @"";
        for (NSDictionary* midiCodeDict in [self _listOfMidiCodesDictionaries]) {
            NSString* name = midiCodeDict[@"name"];
            NSString* databaseMidiCode = midiCodeDict[@"midi_codes"];
            if ([self _value:imageMidiCodes.length fuzzyEqualToValue:databaseMidiCode.length variance:databaseMidiCode.length*0.35] == NO) {
                continue;
            }
            size_t dist = levenshteinDistance([databaseMidiCode UTF8String], databaseMidiCode.length, [imageMidiCodes UTF8String], imageMidiCodes.length);
            if (dist < min_dist) {
                min_dist = dist;
                predictedName = name;
            }
        }
        
        MusicSheetRecognitionResult* result = [[MusicSheetRecognitionResult alloc] init];
        result.predictedImageName = predictedName;
        result.distance = min_dist;
        result.imagesPipline = [_imagesPipline copy];
        result.noteHeadPositions = _noteHeads;
        result.time = ([NSDate date].timeIntervalSinceNow - startTime.timeIntervalSinceNow);
        
        if (predictedName.length == 0) {
            if ([NSThread isMainThread]) {
                if (errorBlock) {
                    errorBlock([NSError errorWithDomain:@"" code:0 userInfo:@{NSLocalizedDescriptionKey:@"Something went wrong."}], result);
                }
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (errorBlock) {
                        errorBlock([NSError errorWithDomain:@"" code:0 userInfo:@{NSLocalizedDescriptionKey:@"Something went wrong."}], result);
                    }
                });
            }
        } else {
            if ([NSThread isMainThread]) {
                if (completionBlock) {
                    if (completionBlock) {
                        completionBlock(result);
                    }
                }
            } else {
                if (completionBlock) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (completionBlock) {
                            completionBlock(result);
                        }
                    });
                }
            }
        }
    }];
}

#pragma mark - Utilz

- (std::vector<cv::KeyPoint>)_detectPossibleNoteHeadsPositions:(cv::Mat)image staff:(Staff *)staff originial:(cv::Mat)musicSheetOriginal {
    cv::SimpleBlobDetector::Params params;
    params.filterByCircularity = TRUE;
    params.minCircularity = 0.1;
    params.filterByConvexity = TRUE;
    params.minConvexity = 0.1;
    params.filterByInertia = TRUE;
    params.minInertiaRatio = 0.01;
    
    std::vector<cv::KeyPoint> keypoints;
    cv::SimpleBlobDetector detector(params);
    
    detector.detect(image, keypoints);
    
    cv::Mat im_with_keypoints = musicSheetOriginal.clone();
    drawKeypoints(musicSheetOriginal, keypoints, im_with_keypoints, cv::Scalar(0,255,0), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS );
    [_imagesPipline addObject:[UIImage imageWithCVMat:im_with_keypoints]];
    cv::imwrite("/Users/brekhunchenko/Desktop/recognition_pipline_ios/possible_note_head_pos.png", im_with_keypoints);
    
    std::vector<cv::KeyPoint> filteredPossibleNoteHeadsPositions;
    for (int i = 0; i < keypoints.size(); i++) {
        cv::KeyPoint p = keypoints[i];
        if ([self _value:p.size fuzzyEqualToValue:staff.spacing variance:(staff.spacing - staff.thickness)*0.65]) {
            filteredPossibleNoteHeadsPositions.push_back(p);
        }
    }
    
    drawKeypoints(musicSheetOriginal, filteredPossibleNoteHeadsPositions, im_with_keypoints, cv::Scalar(0,255,0), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS );
    [_imagesPipline addObject:[UIImage imageWithCVMat:im_with_keypoints]];
    cv::imwrite("/Users/brekhunchenko/Desktop/recognition_pipline_ios/possible_note_head_pos_filtered.png", im_with_keypoints);
    
    return filteredPossibleNoteHeadsPositions;
}

- (NSArray <NoteHead *> *)_getNoteHeadsFromNoteHeadPositions:(std::vector<cv::Point>)notePositions
                                                       staff:(Staff *)staff image:(cv::Mat)musicSheetOriginal
                                                    progress:(void (^)(CGFloat progress))progressBlock {
    __block NSMutableArray<NoteHead *> *noteHeadsMutable = [NSMutableArray new];
    for (int i = 0; i < notePositions.size(); i++) {
        cv::Point p = notePositions[i];
        int size = staff.spacing*13;
        int subImageXStart = p.x - size/2.0f > 0 ? p.x - size/2.0f : 0.0f;
        int subImageYStart = p.y - size/2.0f > 0 ? p.y - size/2.0f : 0.0f;
        int subImageXEnd = p.x + size/2.0f < musicSheetOriginal.cols ? p.x + size/2.0f : musicSheetOriginal.cols;
        int subImageYEnd = p.y + size/2.0f < musicSheetOriginal.rows ? p.y + size/2.0f : musicSheetOriginal.rows;
        
        cv::Rect rect(subImageXStart, subImageYStart, subImageXEnd - subImageXStart, subImageYEnd - subImageYStart);
        cv::Mat subImage(musicSheetOriginal, rect);
//        cv::imwrite("/Users/brekhunchenko/Desktop/recognition_pipline_ios/subImage.png", subImage);
        Staff* subImageStaff = [_linesDetector detectStaffLinesFromImage:subImage.clone() meanThreshold:3.0f];
        if (subImageStaff.tops.count == 0) {
            NSArray* listOfAngles = @[@(0.3), @(-0.3), @(0.6), @(-0.6), @(-1), @(1), @(1.3), @(-1.3), @(2), @(-2), @(-3), @(3)];
            for (NSNumber* angle in listOfAngles) {
                cv::Mat rotatedOriginalImage;
                cv::Mat r = cv::getRotationMatrix2D(p, [angle floatValue], 1.0);
                cv::warpAffine(musicSheetOriginal, rotatedOriginalImage, r, musicSheetOriginal.size());
                cv::Mat rotatedSubImage(rotatedOriginalImage, rect);
                subImageStaff = [_linesDetector detectStaffLinesFromImage:rotatedSubImage.clone() meanThreshold:3.0f];
                if (subImageStaff.tops.count) {
                    break;
                }
            }
        }
        
        if (subImageStaff.tops.count) {
            NSInteger topClosest = NSIntegerMax;
            for (NSNumber* top in subImageStaff.tops) {
                if (top.integerValue < topClosest) {
                    topClosest = top.integerValue;
                }
            }
            
            NSInteger topClosestConverted = p.y - (size/2 - topClosest);
            NSInteger topClosestGlobal = topClosestConverted;
            for (NoteHead* noteHead in noteHeadsMutable) {
                if ([self _value:noteHead.topClosestGlobal fuzzyEqualToValue:topClosestConverted variance:staff.spacing*3.0f]) {
                    topClosestGlobal = noteHead.topClosestGlobal;
                }
            }
            
            NoteHead* noteHead = [[NoteHead alloc] initWithNoteHeadPosition:CGPointMake(p.x, p.y) staff:staff topClosestGlobal:topClosestGlobal topClosestConverted:topClosestConverted];
            [noteHeadsMutable addObject:noteHead];
        }
        
        if (i%15 == 0) {
            if (progressBlock) {
                progressBlock((float)i/notePositions.size());
            }
        }
    }
                   
    for (int i = 0; i < noteHeadsMutable.count; i++) {
        for (int j = 0; j < noteHeadsMutable.count - 1; j++) {
            NoteHead* nh0 = noteHeadsMutable[j];
            NoteHead* nh1 = noteHeadsMutable[j + 1];
            if (nh0.topClosestGlobal == nh1.topClosestGlobal && nh0.noteHeadPosition.x < nh1.noteHeadPosition.x) {
                NoteHead* buffer = nh0;
                noteHeadsMutable[j] = nh1;
                noteHeadsMutable[j + 1] = buffer;
            }
        }
    }
    
    NSMutableIndexSet* indexesForDeleting = [NSMutableIndexSet new];
    for (int i = 0; i < noteHeadsMutable.count; i++) {
        NoteHead* noteHead_0 = noteHeadsMutable[i];
        for (int j = i + 1; j < noteHeadsMutable.count; j++) {
            NoteHead* noteHead_1 = noteHeadsMutable[j];
            if (noteHead_0.topClosestGlobal == noteHead_1.topClosestGlobal && [self _value:noteHead_1.noteHeadPosition.x fuzzyEqualToValue:noteHead_0.noteHeadPosition.x variance:staff.spacing]) {
                [indexesForDeleting addIndex:j];
            }
        }
    }
    [noteHeadsMutable removeObjectsAtIndexes:indexesForDeleting];
    
    [noteHeadsMutable sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return NSOrderedDescending;
    }];
    
    return noteHeadsMutable;
}

//- (cv::Mat)_convolvImageWithNoteHeadsFilter:(cv::Mat)proccessing meanThreshold:(int)meanThreshold staff:(Staff *)staff {
//    cv::adaptiveThreshold(proccessing, proccessing, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C, CV_THRESH_BINARY, 99, 10);
//    proccessing = ~proccessing;
//
//#ifdef DEBUG_MUSIC_SHEETS_RECOGNITION
//    [_imagesPipline addObject:[UIImage imageWithCVMat:proccessing]];
//    cv::imwrite("/Users/brekhunchenko/Desktop/recognition_pipline_ios/proccessing.png", proccessing);
//#endif
//
//    proccessing.convertTo(proccessing, CV_32F);
//    int kernelSize = staff.spacing*1.2;
//    cv::Mat kernel = cv::Mat::ones(kernelSize, kernelSize, CV_32F);
//    cv::Mat conv;
//    cv::filter2D(proccessing, conv, -1 , kernel, cv::Point(-1,-1), 0.0f, cv::BORDER_CONSTANT );
//
//#ifdef DEBUG_MUSIC_SHEETS_RECOGNITION
//    [_imagesPipline addObject:[UIImage imageWithCVMat:conv]];
//    cv::imwrite("/Users/brekhunchenko/Desktop/recognition_pipline_ios/conv.png", conv);
//#endif
//
//    cv::Mat matchHighlightImage = cv::Mat::ones(conv.rows, conv.cols, CV_8U)*255;
//    float mean = cv::mean(conv).val[0];
//    for (int i = 0; i < conv.cols; i++) {
//        for (int j = 0; j < conv.rows; j++) {
//            float value = (float)(conv.at<float>(j, i));
//            if (value > mean*meanThreshold) {
//                cv::Rect rect(i, j, 1, 1);
//                cv::rectangle(matchHighlightImage, rect, cv::Scalar(0, 0, 0), -1);
//            }
//        }
//    }
//
//#ifdef DEBUG_MUSIC_SHEETS_RECOGNITION
//    [_imagesPipline addObject:[UIImage imageWithCVMat:matchHighlightImage]];
//    cv::imwrite("/Users/brekhunchenko/Desktop/recognition_pipline_ios/match_highl.png", matchHighlightImage);
//#endif
//
//    return matchHighlightImage;
//}

- (BOOL)_value:(CGFloat)value0 fuzzyEqualToValue:(CGFloat)value1 variance:(CGFloat)var{
    if(value0 - var <= value1 && value1 <= value0 + var)
        return YES;
    return NO;
}

- (NSString *)_getMidiCodesFromNoteHeads:(NSArray<NoteHead *> *)noteHeads staff:(Staff *)staff {
    NSMutableString* midiCodes = [NSMutableString new];
    CGFloat prevNoteHeadPitch = 0.0;
    for (int i = 0; i < noteHeads.count; i++) {
        NoteHead* noteHead = noteHeads[i];
        NSString* midiCode = @"1";

        CGFloat center = (CGFloat)noteHead.topClosestConverted + staff.spacing*2.0 + staff.thickness/2.0;
        CGFloat notePitch = noteHead.noteHeadPosition.y - center;
        if (i != 0) {
            if ([self _value:notePitch fuzzyEqualToValue:prevNoteHeadPitch variance:staff.thickness]) {
                midiCode = @"1";
            } else if (prevNoteHeadPitch < notePitch) {
                midiCode = @"0";
            } else if (prevNoteHeadPitch > notePitch) {
                midiCode = @"2";
            }
        }
        midiCodes = [[midiCodes stringByAppendingString:midiCode] mutableCopy];
        prevNoteHeadPitch = notePitch;
    }
    return midiCodes;
}

size_t levenshteinDistance(const char* s, size_t n, const char* t, size_t m) {
  ++n; ++m;
  size_t* d = new size_t[n * m];
  
  memset(d, 0, sizeof(size_t) * n * m);
  
  for (size_t i = 1, im = 0; i < m; ++i, ++im) {
    for (size_t j = 1, jn = 0; j < n; ++j, ++jn) {
      if (s[jn] == t[im]) {
        d[(i * n) + j] = d[((i - 1) * n) + (j - 1)];
      } else {
        d[(i * n) + j] = std::min(d[(i - 1) * n + j] + 1, std::min(d[i * n + (j - 1)] + 1, d[(i - 1) * n + (j - 1)] + 1));
      }
    }
  }
  
  size_t r = d[n * m - 1];
  
  delete [] d;
  
  return r;
}

- (NSArray<NSDictionary *> *)_listOfMidiCodesDictionaries {
  static NSArray<NSDictionary *> *listOfMidiCodesDictionaries = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"midi_codes" ofType:@"json"];
    NSError * error = nil;
    NSString *jsonString = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    NSData * jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    listOfMidiCodesDictionaries = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
  });
  return listOfMidiCodesDictionaries;
}

@end

@implementation MusicSheetRecognitionResult

@end
