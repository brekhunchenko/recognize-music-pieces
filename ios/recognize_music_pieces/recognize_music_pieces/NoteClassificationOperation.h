//
//  NoteRecognitionRequest.h
//  recognize_music_pieces
//
//  Created by Yaroslav Brekhunchenko on 10/19/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol NoteClassificationOperationDelegate;

@interface NoteClassificationOperation : NSOperation

- (instancetype)initWithImage:(UIImage *)image notePosition:(CGPoint)notePosition;
@property (nonatomic, strong, readonly) UIImage* noteHeadImage;
@property (nonatomic, assign, readonly) CGPoint notePosition;

@property (nonatomic, weak) id<NoteClassificationOperationDelegate> delegate;

//Value from 0.0 to 1.0. Default is 0.9.
@property (nonatomic, assign) CGFloat recognitionConfidenceThreshold;

@end

@protocol NoteClassificationOperationDelegate

- (void)noteClassificationOperationDidFinish:(NoteClassificationOperation *)request isNote:(BOOL)isNote;

@end
