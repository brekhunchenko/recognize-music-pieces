//
//  UIImage+Utilz.h
//  TextDeblur
//
//  Created by Yaroslav Brekhunchenko on 7/26/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Utilz)

+ (UIImage*)imageWithColor:(UIColor*)color size:(CGSize)size;

+ (UIImage*)imageWithImage:(UIImage*)sourceImage scaledToWidth:(float)i_width;

- (UIImage *)crop:(CGRect)rect;

@end
