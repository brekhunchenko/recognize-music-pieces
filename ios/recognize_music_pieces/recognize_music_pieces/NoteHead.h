//
//  NoteHead.h
//  recognize_music_pieces
//
//  Created by Yaroslav Brekhunchenko on 8/3/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import "Staff.h"

@interface NoteHead : NSObject

- (instancetype)initWithNoteHeadPosition:(CGPoint)noteHeadPosition staff:(Staff *)staff topClosestGlobal:(NSInteger)topClosestGlobal topClosestConverted:(NSInteger)topClosestConverted;

@property (nonatomic, assign, readonly) CGPoint noteHeadPosition;
@property (nonatomic, strong, readonly) Staff* staff;
@property (nonatomic, assign, readonly) NSInteger topClosestGlobal;
@property (nonatomic, assign, readonly) NSInteger topClosestConverted;

@end
