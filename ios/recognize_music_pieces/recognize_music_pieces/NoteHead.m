//
//  NoteHead.m
//  recognize_music_pieces
//
//  Created by Yaroslav Brekhunchenko on 8/3/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import "NoteHead.h"

@interface NoteHead()

@end

@implementation NoteHead

#pragma mark - Initializers

- (instancetype)initWithNoteHeadPosition:(CGPoint)noteHeadPosition staff:(Staff *)staff topClosestGlobal:(NSInteger)topClosestGlobal topClosestConverted:(NSInteger)topClosestConverted {
  self = [super init];
  if (self) {
    _noteHeadPosition = noteHeadPosition;
    _staff = staff;
    _topClosestGlobal = topClosestGlobal;
    _topClosestConverted = topClosestConverted;
  }
  return self;
}

@end
