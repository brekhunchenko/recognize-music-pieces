//
//  NoteRecognitionRequest.m
//  recognize_music_pieces
//
//  Created by Yaroslav Brekhunchenko on 10/19/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import "NoteClassificationOperation.h"
#import "NoteHeadsRecognitionModel.h"

#import <CoreML/CoreML.h>
#import <Vision/Vision.h>

@interface NoteClassificationOperation()

@property (nonatomic, assign) BOOL done;

@end

@implementation NoteClassificationOperation

#pragma mark - Initialziers

- (instancetype)initWithImage:(UIImage *)image notePosition:(CGPoint)notePosition {
    NSParameterAssert(image);
    
    self = [super init];
    if (self) {
        _noteHeadImage = image;
        _notePosition = notePosition;
        
        _recognitionConfidenceThreshold = 0.9;
    }
    return self;
}

#pragma mark - NSOperation Overridden

- (BOOL)isFinished {
    return [super isFinished] && self.done;
}

- (void)cancel {
    [super cancel];
}

- (void)main {
    MLModel *model = [[[NoteHeadsRecognitionModel alloc] init] model];
    VNCoreMLModel *m = [VNCoreMLModel modelForMLModel:model error:nil];
    
    __weak typeof(self) weakSelf = self;
    VNCoreMLRequest *rq = [[VNCoreMLRequest alloc] initWithModel:m completionHandler:(VNRequestCompletionHandler)^(VNRequest *request, NSError *error){
        dispatch_async(dispatch_get_main_queue(), ^{
            VNClassificationObservation *topResult = ((VNClassificationObservation *)([request.results firstObject]));
            
            [weakSelf willChangeValueForKey:@"isFinished"];
            weakSelf.done = YES;
            [weakSelf didChangeValueForKey:@"isFinished"];
            
            BOOL isNote = ![topResult.identifier isEqualToString:@"background"] && topResult.confidence > weakSelf.recognitionConfidenceThreshold;
            [weakSelf.delegate noteClassificationOperationDidFinish:weakSelf isNote:isNote];
        });
    }];
    rq.preferBackgroundProcessing = NO;
    
    VNImageRequestHandler* sequence = [[VNImageRequestHandler alloc] initWithCIImage:[CIImage imageWithCGImage:_noteHeadImage.CGImage]
                                                                             options:@{}];
    [sequence performRequests:@[rq] error:nil];
}

@end
