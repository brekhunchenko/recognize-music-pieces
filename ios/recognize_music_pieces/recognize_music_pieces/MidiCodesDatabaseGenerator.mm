//
//  MidiCodesDatabaseGenerator.m
//  recognize_music_pieces
//
//  Created by Yaroslav Brekhunchenko on 10/20/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import "MidiCodesDatabaseGenerator.h"
#import "MusicSheetsRecognizer.h"

@interface MidiCodesDatabaseGenerator() < MidiCodeFromMusicSheetImageOperationDelegate >

@property (atomic, strong) NSMutableArray* midiCodesDatabase;

@end

@implementation MidiCodesDatabaseGenerator

#pragma mark - Initializers

- (instancetype) init {
    self = [super init];
    if (self) {
        _midiCodesDatabase = [NSMutableArray new];
    }
    return self;
}

#pragma mark - Public Methods

- (void)createDefaultMusicCodesDatabase {
    NSArray* listOfMusicSheets = @[
                                   @"allegro.jpg",
                                   @"allegro_2.jpg",
                                   @"america.jpg",
                                   @"andante.jpg",
                                   @"andante_2.jpg",
                                   @"bathwater_blues.jpg",
                                   @"bathwater_blues_2.jpg",
                                   @"dream.jpg",
                                   @"ecossaise.jpg",
                                   @"elenke.jpg",
                                   @"greek_wedding.jpg",
                                   @"ground_after_the_scotch_humour.jpg",
                                   @"ground_after_the_scotch_humour_2.jpg",
                                   @"intermezzo.jpg",
                                   @"intermezzo_2.jpg",
                                   @"lhomme_arme.jpg",
                                   @"minuet.jpg",
                                   @"over_the_rainbow.jpg",
                                   @"pastime_with_good_company.jpg",
                                   @"pig_ankle_rag.jpg",
                                   @"pitlochry.jpg",
                                   @"polonaise.jpg",
                                   @"presto.jpg",
                                   @"romance.jpg",
                                   @"romance_2.jpg",
                                   @"rondino.jpg",
                                   @"rondino_2.jpg",
                                   @"stoppin_off_in_louisiana.jpg",
                                   @"the_folk_from_the_mountain.jpg",
                                   @"the_folk_from_the_mountain_2.jpg",
                                   @"the_grey_dove.jpg",
                                   @"the_holy_boy.jpg",
                                   @"the_holy_boy_2.jpg",
                                   @"the_lark_in_the_clear_air.jpg",
                                   @"the_muppet_show_theme.jpg",
                                   @"theme_and_vatiation.jpg",
                                   ];
    NSString* saveJSONPath = @"/Users/brekhunchenko/Documents/Projects/Recognize_Music_Pieces/Midi_codes/midi_codes.json";
    [[[MidiCodesDatabaseGenerator alloc] init] createMusicCodesDatabaseForListOfImages:listOfMusicSheets savePath:saveJSONPath];
}

- (void)createMusicCodesDatabaseForListOfImages:(NSArray *)listOfMusicSheets savePath:(NSString *)saveJSONPath {
    [_midiCodesDatabase removeAllObjects];

    NSOperationQueue* recognitionOperationsQueue = [[NSOperationQueue alloc] init];
    recognitionOperationsQueue.maxConcurrentOperationCount = 1;
    NSMutableArray<__kindof NSOperation *>* operationsMutable = [NSMutableArray new];

    for (NSString* imageName in listOfMusicSheets) {
        MidiCodeFromMusicSheetImageOperation* midiCodeOperation = [[MidiCodeFromMusicSheetImageOperation alloc] initWithMusicSheetImageName:imageName];
        midiCodeOperation.delegate = self;
        MidiCodeFromMusicSheetImageOperation* previousOperation = [operationsMutable lastObject];
        if (previousOperation) {
            [midiCodeOperation addDependency:previousOperation];
        }
        [operationsMutable addObject:midiCodeOperation];
    }
    
    NSBlockOperation* finishedCallbackOperation = [NSBlockOperation blockOperationWithBlock:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            NSData *json1 = [NSJSONSerialization dataWithJSONObject:_midiCodesDatabase options:NSJSONWritingPrettyPrinted error:nil];
            [json1 writeToFile:saveJSONPath atomically:YES];
        });
    }];
    
    MidiCodeFromMusicSheetImageOperation* lastOperation = [operationsMutable lastObject];
    [finishedCallbackOperation addDependency:lastOperation];
    [operationsMutable addObject:finishedCallbackOperation];
    
    [recognitionOperationsQueue addOperations:operationsMutable waitUntilFinished:NO];
}

#pragma mark - MidiCodeFromMusicSheetImageOperationDelegate

- (void)midiCodesFromMusicSheetImageOperatinoDidFinish:(MidiCodeFromMusicSheetImageOperation *)operation midiCode:(NSString *)midiCodes {
    NSLog(@"Generated midi codes for image: %@, midiCodes: %@", operation.imageName, midiCodes);
    [_midiCodesDatabase addObject:@{@"name":operation.imageName, @"midi_codes":midiCodes}];
}

@end

@interface MidiCodeFromMusicSheetImageOperation()

@property (nonatomic, assign) BOOL done;
@property (nonatomic, strong) MusicSheetsRecognizer* musicSheetRecognizer;

@end

@implementation MidiCodeFromMusicSheetImageOperation

#pragma mark - Initializers

- (instancetype)initWithMusicSheetImageName:(NSString *)imageName {
    self = [super init];
    if (self) {
        _imageName = imageName;
        
        NSString *imagePath = [[NSBundle mainBundle] pathForResource:_imageName ofType:@""];
        cv::Mat musicSheetOriginal = cv::imread(imagePath.fileSystemRepresentation, cv::IMREAD_GRAYSCALE);

        NSAssert(musicSheetOriginal.cols != 0 || musicSheetOriginal.rows != 0, @"");
        _musicSheetOriginal = musicSheetOriginal;
        
        _musicSheetRecognizer = [[MusicSheetsRecognizer alloc] init];
    }
    return self;
}


#pragma mark - NSOperation Overridden

- (BOOL)isFinished {
    return [super isFinished] && self.done;
}

- (void)cancel {
    [super cancel];
}

- (BOOL)isAsynchronous {
    return NO;
}

- (void)main {
    dispatch_async(dispatch_get_main_queue(), ^{
        __weak typeof(self) weakSelf = self;
        [_musicSheetRecognizer midiCodeStringFromImage:_musicSheetOriginal progress:nil completion:^(NSString *midiCodes) {
            [weakSelf willChangeValueForKey:@"isFinished"];
            weakSelf.done = YES;
            [weakSelf didChangeValueForKey:@"isFinished"];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.delegate midiCodesFromMusicSheetImageOperatinoDidFinish:weakSelf midiCode:midiCodes];
            });
        }];
    });
}

@end

