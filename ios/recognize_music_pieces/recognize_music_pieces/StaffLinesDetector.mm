//
//  StaffLinesDetector.m
//  recognize_music_pieces
//
//  Created by Yaroslav Brekhunchenko on 6/19/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import "StaffLinesDetector.h"
#include <opencv2/core/core.hpp>
#import <opencv2/opencv.hpp>
#import <opencv2/imgproc/imgproc_c.h>
#import <opencv2/ml/ml.hpp>

@interface StaffLinesDetector()

@end

@implementation StaffLinesDetector

#pragma mark - Initializers

- (Staff *)detectStaffLinesFromImage:(cv::Mat)image meanThreshold:(float)meanThreshold {
    //    cv::Mat drawing = image.clone();
    //    cv::cvtColor(drawing, drawing, cv::COLOR_GRAY2BGR);
    
    cv::adaptiveThreshold(image, image, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C, CV_THRESH_BINARY, 99, 10);
    if (image.cols > 300) {
        cv::imwrite("/Users/brekhunchenko/Desktop/recognition_pipline_ios/binary.jpg", image);
    }
    int thickness = [self _verticalRunLengthMode:image destinationColor:0];
    int spacing = [self _verticalRunLengthMode:image destinationColor:255] + thickness;
    //    NSLog(@"Staff bar thickness: %d, line spacing: %d", thickness, spacing);
  
    if (thickness < 5) {
        cv::Mat kernel = cv::Mat::ones(2, 2, CV_32F);
        cv::erode(image, image, kernel);
        if (image.cols > 300) {
            cv::imwrite("/Users/brekhunchenko/Desktop/recognition_pipline_ios/dilate.jpg", image);
        }
    }

    NSArray* peaks = [self _foundStaffLinesPeaks:image thickness:thickness spacing:spacing meanThreshold:meanThreshold];
    NSArray* tops = [self _foundTopsFromStaffLinePeaks:peaks thickness:thickness spacing:spacing];
  
    Staff * staff = [[Staff alloc] initWithStaffWithLineThickness:thickness spacing:spacing peaks:peaks tops:tops];
    return staff;
}

- (Staff *)detectStaffLinesFromImage:(cv::Mat)image {
    return [self detectStaffLinesFromImage:image meanThreshold:2.0f];
}

- (cv::Mat)cropStaffLinesFromImage:(cv::Mat)image staffTop:(NSNumber *)staffTop thickness:(int)thickness spacing:(int)spacing {
  int staffCenter = staffTop.intValue + + thickness*2.0f + spacing*2.0f + thickness/2.0;
  int offset = spacing*6.0f;
  int cropStart = (staffCenter - offset) > 0 ? (staffCenter - offset) : 0;
  int cropEnd = (staffCenter + offset) < image.rows ? (staffCenter + offset) : image.rows;
  cv::Mat cropedImage = image(cv::Rect(0.0f, cropStart, image.cols, (cropEnd - cropStart)));
  return cropedImage;
}

#pragma mark - Utilz

- (int)_verticalRunLengthMode:(cv::Mat)image destinationColor:(int)color {
    NSInteger width = image.size().width;
    NSInteger height = image.size().height;
    int startIndex = (int)(width/4);
    int endIndex = (int)(width*(3.0/4.0));
    int step = (int)(width*0.05);
    NSMutableArray* runLengths = [NSMutableArray new];
    for (int i = startIndex; i < endIndex; i+=step) {
        BOOL inColor = NO;
        int currentRun = 0;
        for (int j = 0; j < height; j++) {
            uchar value = (uchar)(image.at<uchar>(j, i));
            if (value == color) {
                if (inColor) {
                    currentRun = currentRun + 1;
                } else {
                    currentRun = 1;
                    inColor = YES;
                }
            } else {
                if (inColor) {
                    [runLengths addObject:@(currentRun)];
                    inColor = NO;
                }
            }
        }
    }

    NSCountedSet* numberSet = [[NSCountedSet alloc] initWithArray:runLengths];
    NSNumber* mostCommon = nil;
    NSUInteger highestCount = 0;

    for (NSNumber* number in numberSet) {
        NSUInteger count = [numberSet countForObject:number];
        if(count > highestCount) {
            highestCount = count;
            mostCommon = number;
        }
    }

    return mostCommon.intValue;
}

- (NSArray *)_foundStaffLinesPeaks:(cv::Mat)image thickness:(int)thickness spacing:(int)staffLineSpacing meanThreshold:(float)meanThreshold {
    image = 255 - image;

    int width = image.cols;
    int height = image.rows;
    NSMutableArray* h_projection = [NSMutableArray new];
    for (int i = 0; i < height; i++) {
        float sum = 0;
        for (int j = 0; j < width; j++) {
            uchar value = (uchar)(image.at<uchar>(i, j));
            sum += ((float)value)/255.0f;
        }
        [h_projection addObject:@(sum)];
    }

    CGFloat mean = [self _mean:h_projection].floatValue;
    NSMutableArray* peaks = [NSMutableArray new];
    for (int i = 0; i < h_projection.count; i++) {
        CGFloat pixel = [h_projection[i] floatValue];
        BOOL peak = YES;
        CGFloat width = int(staffLineSpacing*1.3);
        for (int neighbor_index = -width/2; neighbor_index < width/2; neighbor_index++) {
            if (i + neighbor_index > 0 && i + neighbor_index < h_projection.count) {
                CGFloat neighbor_value = [h_projection[i + neighbor_index] floatValue];
                if (neighbor_value > pixel) {
                    peak = NO;
                    break;
                }
            }
        }

        if (peak && pixel > mean*meanThreshold) {
            [peaks addObject:@(i)];
        }
    }

    return peaks;
}

- (NSArray *)_foundTopsFromStaffLinePeaks:(NSArray *)peaks thickness:(int)thickness spacing:(int)spacing {
    NSMutableArray* tops = [NSMutableArray new];
    for (NSNumber* peak in peaks) {
        if ([self _isTopStaffLine:peak peaks:peaks gap:spacing threshold:thickness]) {
            [tops addObject:peak];
        }
    }
    return tops;
}

- (BOOL)_isTopStaffLine:(NSNumber *)peak peaks:(NSArray *)peaks gap:(int)gap threshold:(int)threshold {
    for (int k = 0; k < peaks.count; k++) {
        NSInteger rhoValue = [peaks[k] integerValue];
        if (rhoValue == [peak integerValue]) {
            int counter = 0;
            for (int i = 1; i < 5; i++) {
                if (k + i < peaks.count) {
                    NSInteger neighbor_rho = [peaks[k + i] integerValue];
                    if ([self _value:neighbor_rho fuzzyEqualToValue:(peak.integerValue + i*gap) variance:threshold]) {
                        counter++;
                    }
                }
            }
            
            return (counter == 4);
        }
    }
    return NO;
}

- (BOOL)_value:(CGFloat)value0 fuzzyEqualToValue:(CGFloat)value1 variance:(CGFloat)var{
    if(value0 - var <= value1 && value1 <= value0 + var)
        return YES;
    return NO;
}

- (NSNumber *)_mean:(NSArray *)array {
    double runningTotal = 0.0;
    for(NSNumber *number in array) {
        runningTotal += [number doubleValue];
    }
    return [NSNumber numberWithDouble:(runningTotal/[array count])];
}

- (NSArray *)possibleRegionsForNoteHeadsFromStaffImage:(cv::Mat)staffBarImage staff:(Staff *)staff {
    cv::adaptiveThreshold(staffBarImage, staffBarImage, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C, CV_THRESH_BINARY, 99, 10);
    staffBarImage = ~staffBarImage;

    NSInteger width = staffBarImage.cols;
    NSInteger height = staffBarImage.rows;
    NSMutableArray* v_projection = [NSMutableArray new];
    for (int i = 0; i < width; i++) {
        float sum = 0;
        for (int j = 0; j < height; j++) {
            int value = (int)(staffBarImage.at<cv::Vec3b>(i, j)[0]);
            sum += float(value)/255.0f;
        }
        [v_projection addObject:@(sum)];
    }

    CGFloat mean = [self _mean:v_projection].floatValue;
    NSMutableArray* peaks = [NSMutableArray new];
    for (int i = 0; i < v_projection.count; i++) {
        CGFloat pixel = [v_projection[i] floatValue];
        BOOL peak = YES;
        CGFloat width = int(staff.spacing);
        for (int neighbor_index = -width/2; neighbor_index < width/2; neighbor_index++) {
            if (i + neighbor_index > 0 && i + neighbor_index < v_projection.count) {
                CGFloat neighbor_value = [v_projection[i + neighbor_index] floatValue];
                if (neighbor_value > pixel) {
                  peak = NO;
                  break;
                }
            }
        }

        if (peak && pixel > mean) {
            [peaks addObject:@(i)];
        }
    }

    return peaks;
}

@end
