import cv2
import numpy as np
from scipy import ndimage
# from simmilarity_images import dist
import math
import staff_data
import json
from pprint import pprint
import os

def numberOfFilesInFolder(folder_path):
    file_cnt = len([f for f in os.listdir(folder_path) if os.path.isfile(os.path.join(folder_path, f))])
    return file_cnt

def dist(a, b):
    return math.sqrt((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2)

def noteNearPoint(point, note_heads_positions):
    for note in note_heads_positions:
        if dist(point, note) < float(note_img_size * 0.7):
            return True

    return False

def cropNoteHeads():
    for i in range(0, len(note_heads_positions)):
        note_pos = note_heads_positions[i]
        start_x = int((int(note_pos[0]) - note_img_size/2))
        end_x = int((int(note_pos[0]) + note_img_size/2))
        start_y = int((int(note_pos[1]) - note_img_size/2))
        end_y = int((int(note_pos[1]) + note_img_size/2))
        sub_im = img[start_y:end_y, start_x:end_x]
        sub_im = cv2.resize(sub_im, (neural_net_img_size, neural_net_img_size))

        path = dir + "solid_note_head/" + ("%d.jpg" % (i + note_index_offset))
        cv2.imwrite(path, sub_im)

def cropBackgrounds():
    counter = 0
    cimg = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    copyOverlay = cimg.copy()

    for i in range(int(note_img_size), img.shape[1], 20):
        for j in range(int(note_img_size), img.shape[0], 20):
            start_x = int(i - note_img_size / 2)
            end_x = int(i + note_img_size / 2)
            start_y = int(j - note_img_size / 2)
            end_y = int(j + note_img_size / 2)

            note_nearly = noteNearPoint((i, j), note_heads_positions)

            if note_nearly == False:
                sub_im = img[start_y:end_y, start_x:end_x]
                path = dir + "background/" + ("%d.jpg" % (counter + background_index_offset))
                sub_im = cv2.resize(sub_im, (neural_net_img_size, neural_net_img_size))

                if ndimage.variance(sub_im) > 1000:
                    cv2.imwrite(path, sub_im)

                    counter = counter + 1

                    cv2.rectangle(copyOverlay, (start_x, start_y), (i + neural_net_img_size, j + neural_net_img_size), (0, 0, 255), -1)

    cimg = cv2.addWeighted(copyOverlay, 0.3, cimg, 0.7, 0)

    cv2.imwrite((dir + ("%s_background.jpg" % img_name)), cimg)


list_of_training_image_names = ['IMG_6216.JPG',
 'IMG_6217.JPG',
 'IMG_6218.JPG',
 'IMG_6219.JPG',
 'IMG_6220.JPG',
 'IMG_6283.JPG',
 'IMG_6284.JPG',
 'IMG_6285.JPG',
 'IMG_6286.JPG',
 'IMG_6288.JPG',
 'IMG_6366.JPG',
 'IMG_6367.JPG',
 'IMG_6368.JPG',
 'IMG_6369.JPG',
 'IMG_6370.JPG',
 'IMG_6371.JPG',
 'IMG_6373.JPG',
 'IMG_6374.JPG',
 'IMG_6377.JPG',
 'IMG_6378.JPG',
 'IMG_6381.JPG',
 'IMG_6382.JPG']

for i in range(len(list_of_training_image_names)):
    img_name = list_of_training_image_names[i]
    points_json_path = '/Users/brekhunchenko/Projects/recognize_music_pieces/photos_database/json_note_heads/%s.json' % img_name
    img_path = "/Users/brekhunchenko/Projects/recognize_music_pieces/photos_database/photos/%s" % img_name
    dir = "/Users/brekhunchenko/Desktop/training_set/"

    note_heads_positions = []
    with open(points_json_path) as data_file:
        data = json.load(data_file)

    for point in data['points']:
        note_heads_positions.append((point['x'], point['y']))

    img = cv2.imread(img_path, 0)

    staffData = staff_data.getStaffData(img)
    note_head_size = 11
    scale = float(note_head_size)/staffData.lineSpacing

    neural_net_img_size = 22
    note_img_size = staffData.lineSpacing*2.5

    note_index_offset = numberOfFilesInFolder((dir + "solid_note_head/"))
    background_index_offset = numberOfFilesInFolder((dir + "background/"))

    print 'background_index_offset  %d note_index_offset  %d' % (background_index_offset, note_index_offset)

    cropNoteHeads()
    cropBackgrounds()

# list_of_test_image_names = ['IMG_6287.JPG',  'IMG_6372.JPG',  'IMG_6380.JPG',  'IMG_6282.JPG']
# for i in range(len(list_of_test_image_names)):
#     img_name = list_of_test_image_names[i]
#     points_json_path = '/Users/brekhunchenko/Projects/recognize_music_pieces/photos_database/json_note_heads/%s.json' % img_name
#     img_path = "/Users/brekhunchenko/Projects/recognize_music_pieces/photos_database/photos/%s" % img_name
#     dir = "/Users/brekhunchenko/Desktop/test_set/"
#
#     note_heads_positions = []
#     with open(points_json_path) as data_file:
#         data = json.load(data_file)
#
#     for point in data['points']:
#         note_heads_positions.append((point['x'], point['y']))
#
#     img = cv2.imread(img_path, 0)
#
#     staffData = staff_data.getStaffData(img)
#     note_head_size = 11
#     scale = float(note_head_size)/staffData.lineSpacing
#
#     neural_net_img_size = 22
#     note_img_size = staffData.lineSpacing*2.5
#
#     note_index_offset = numberOfFilesInFolder((dir + "solid_note_head/"))
#     background_index_offset = numberOfFilesInFolder((dir + "background/"))
#
#     print 'background_index_offset  %d note_index_offset  %d' % (background_index_offset, note_index_offset)
#
#     cropNoteHeads()
#     cropBackgrounds()