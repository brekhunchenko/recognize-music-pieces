import numpy as np
from scipy.signal import fftconvolve
import cv2
import musical_object_classes
import math
from scipy import stats
import matplotlib.pyplot as plt
import pylab
from scipy import signal
import matplotlib.pyplot as plt
import numpy as np
import plotly.plotly as py
import correct_midi_codes
import sys
import staff_data
from neural_network import train_cnn, detect_note_heads_positions, is_note_image, pixel_vec
import os, errno
import json
import difflib
import codecs

def lev_dist(source, target):
    if source == target:
        return 0

    slen, tlen = len(source), len(target)
    dist = [[0 for i in range(tlen+1)] for x in range(slen+1)]
    for i in xrange(slen+1):
        dist[i][0] = i
    for j in xrange(tlen+1):
        dist[0][j] = j

    for i in xrange(slen):
        for j in xrange(tlen):
            cost = 0 if source[i] == target[j] else 1
            dist[i+1][j+1] = min(
                            dist[i][j+1] + 1,   # deletion
                            dist[i+1][j] + 1,   # insertion
                            dist[i][j] + cost   # substitution
                        )
    return dist[-1][-1]

def performNoteHeadRecognition(image_name, images_folder = "/Users/brekhunchenko/Desktop/music_sheets/training_set/"):
    dir = "/Users/brekhunchenko/Documents/Projects/Recognize_Music_Pieces/Music_sheets_recognition_pipline/" + image_name
    if not os.path.exists(dir):
        os.makedirs(dir)

    im = cv2.imread(images_folder + image_name, 0)
    im = cv2.resize(im, (1512, 2016))
    cv2.imwrite(dir + "/0_origin_black_and_white.jpg", im)

    staffData = staff_data.getStaffData(im)

    keypoints = []
    for meanThreshold in [7, 10, 12]:
        keypoints_possible = get_possible_image_head_positions(image_name=image_name, meanThreshold=meanThreshold, staffData=staffData, images_folder=images_folder)
        if len(keypoints_possible) > len(keypoints):
            keypoints = keypoints_possible

    im_with_keypoints = cv2.drawKeypoints(im, keypoints, np.array([]), (0, 0, 255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

    cv2.imwrite(dir + "/3_possible_note_head_position_regions.png", im_with_keypoints)

    images = np.empty((0, 64*64), dtype='float32')
    orig_note_image = []
    note_points_positions = []
    for i in range(len(keypoints)):
        keypoint = keypoints[i]
        start_x = int(keypoint.pt[0] - staffData.lineSpacing*1.5)
        end_x = int(keypoint.pt[0] + staffData.lineSpacing*1.5)
        start_y = int(keypoint.pt[1] - staffData.lineSpacing*1.5)
        end_y = int(keypoint.pt[1] + staffData.lineSpacing*1.5)
        sub_im = im[start_y:end_y, start_x:end_x]
        # cv2.imwrite(("/Users/brekhunchenko/Desktop/recognize_pipline/img.png"), sub_im)
        if sub_im.shape[0] > 0 and sub_im.shape[1] > 0:
            images = np.append(images, np.array([pixel_vec(sub_im)]), axis=0)
            orig_note_image.append(sub_im)
            note_points_positions.append(keypoint.pt)

    is_notes = is_note_image(images, threshold=0.7)

    cimg = cv2.cvtColor(im, cv2.COLOR_GRAY2BGR)

    filtered_notes_positions = np.array(note_points_positions, dtype='uint32')[is_notes]
    music_objects = []
    size = staffData.lineSpacing * 13

    for k in range(len(filtered_notes_positions)):
        note_head = filtered_notes_positions[k]

        j, i = note_head
        cv2.rectangle(cimg, (int(j - staffData.lineSpacing), int(i - staffData.lineSpacing)), (int(j + staffData.lineSpacing*1.5), int(i + staffData.lineSpacing*1.5)), (0, 0, 255), 2)

        subImageStaffData = staff_data.getStaffDataForNoteHeadPos(im, staffData, note_head, size)

        topClosest = sys.maxint
        for top in subImageStaffData.tops:
            if (top < topClosest):
                topClosest = top

        topClosestConvertedCoord = i - (size / 2 - topClosest)
        topClosestGlobal = topClosestConvertedCoord
        for music_object in music_objects:
            if np.allclose(music_object.topClosestGlobal, topClosestConvertedCoord, atol=staffData.lineSpacing*3):
                topClosestGlobal = music_object.topClosestGlobal

        if len(subImageStaffData.tops):
            musicalObj = musical_object_classes.MusicalObject('note head', point=note_head, staffData=subImageStaffData, topClosest=topClosestConvertedCoord, topClosestGlobal=topClosestGlobal)
            music_objects.append(musicalObj)

    sorted_music_objects = sorted(music_objects, key=lambda x: (x.topClosestGlobal, x.point[0]))

    cv2.imwrite(dir + "/4_note_heads.jpg", cimg)

    return sorted_music_objects, staffData

def get_possible_image_head_positions(image_name, staffData, meanThreshold = 7, images_folder = "/Users/brekhunchenko/Desktop/music_sheets/training_set/"):
    dir = "/Users/brekhunchenko/Desktop/recognize_pipline/" + image_name
    if not os.path.exists(dir):
        os.makedirs(dir)

    im = cv2.imread(images_folder + image_name, 0)
    cv2.imwrite(dir + "/0_origin_black_and_white.jpg", im)

    binary_im = cv2.adaptiveThreshold(im, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 99, 10)
    binary_im = cv2.bitwise_not(binary_im)
    cv2.imwrite(dir + "/1_binary_staffbar.png", binary_im)

    k = np.ones([staffData.lineThickness * 3, staffData.lineThickness * 3], dtype=float)
    node_detect = cv2.filter2D(np.float32(binary_im), -1, k, borderType = cv2.BORDER_CONSTANT)

    matchHighlightImage = np.ones(binary_im.shape, dtype=np.uint8) * 255
    matchHighlightImage = cv2.cvtColor(matchHighlightImage.copy(), cv2.COLOR_GRAY2BGR)
    locations = np.where(node_detect >= node_detect.mean() * meanThreshold)
    for point in zip(*locations[::-1]):
        cv2.rectangle(matchHighlightImage, point, (point[0] + 3, point[1] + 3), (0, 0, 0), 2)

    cv2.imwrite(dir + "/2_match_staffbar_%d.png", matchHighlightImage)

    params = cv2.SimpleBlobDetector_Params()
    params.filterByCircularity = True
    params.minCircularity = 0.1
    params.filterByConvexity = True
    params.minConvexity = 0.3
    params.filterByInertia = True
    params.minInertiaRatio = 0.01

    detector = cv2.SimpleBlobDetector_create(params)

    keypoints = detector.detect(matchHighlightImage)

    filtered_keypoints = []
    for keypoint in keypoints:
        if keypoint.size > float(staffData.lineSpacing)*0.6 and keypoint.size < float(staffData.lineSpacing)*1.9:
            filtered_keypoints.append(keypoint)

    keypoints = filtered_keypoints

    im_with_keypoints = cv2.drawKeypoints(im, keypoints, np.array([]), (0, 0, 255),
                                          cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

    cv2.imwrite(dir + "/3_possible_note_head_position_regions.png", im_with_keypoints)

    return keypoints

def midi_codes_for_image_paths(folder_path):
    img_paths = []
    for image_name in os.listdir(folder_path):
        if os.path.isfile(os.path.join(folder_path, image_name)) and image_name != ".DS_Store":
            print "@\"%s\"," % image_name
            img_paths.append(image_name)

    music_sheet_codes = []
    for image_name in img_paths:
        music_objects, staffData = performNoteHeadRecognition(image_name, images_folder=folder_path)
        midiCodes = staff_data.getMidiCodes(image_name, staffData, music_objects, images_folder=folder_path)

        music_sheet_codes.append({"name":image_name, "midi_codes":midiCodes})
        print '%s midi codes: %s' % (image_name, str(midiCodes))

    out_file = open((folder_path + "midi_codes.json"),"w")
    json.dump(music_sheet_codes, out_file, indent=4)
    out_file.close()

    return music_sheet_codes


def predict_music_sheet_name(image_name, folder_path):
    music_objects, staffData = performNoteHeadRecognition(image_name, images_folder=folder_path)
    image_midi_codes = staff_data.getMidiCodes(image_name, staffData, music_objects, images_folder=folder_path)

    in_file = open("/Users/brekhunchenko/Documents/Projects/Recognize_Music_Pieces/Midi_codes/midi_codes.json", "r")
    midi_codes = json.load(in_file)
    in_file.close()

    max_ratio = 0
    predicted_image_name = ""

    min_lev_length = 100000

    for midi_code_dict in midi_codes:
        midi_codes = midi_code_dict["midi_codes"]
        name = midi_code_dict["name"]

        lev_distance = lev_dist (midi_codes, image_midi_codes)
        if lev_distance < min_lev_length:
            predicted_image_name = name
            min_lev_length = lev_distance

        # print 'here'

    # print 'orig image name : %s | predicted image name: %s | ratio: %s' % (image_name, predicted_image_name, str(max_ratio))
    print 'orig image name : %s | predicted image name: %s | dist %d' % (image_name, predicted_image_name, min_lev_length)


# midi_codes_for_image_paths("/Users/brekhunchenko/Documents/Projects/Recognize_Music_Pieces/Music_Sheets/")

folder_path = "/Users/brekhunchenko/Downloads/"
predict_music_sheet_name("test_greek_wedding.jpg", folder_path)

im = cv2.imread("/Users/brekhunchenko/Documents/Projects/Recognize_Music_Pieces/Test_music_sheets/test_dream_2.jpg", 0)
im = cv2.resize(im, (1512, 2016))
im = cv2.adaptiveThreshold(im, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 99, 10)
im = 255 - im
kernel = np.ones((3,3),np.uint8)
dilation = cv2.dilate(im,kernel,iterations = 1)
cv2.imwrite("/Users/brekhunchenko/Desktop/recognition_pipline_ios/binary.png", im)
cv2.imwrite("/Users/brekhunchenko/Desktop/recognition_pipline_ios/dilation.png", dilation)

# for image_name in os.listdir(folder_path):
#     if os.path.isfile(os.path.join(folder_path, image_name)) and image_name != ".DS_Store":
#         print "@\"%s\"," % image_name
#         predict_music_sheet_name(image_name, folder_path)
