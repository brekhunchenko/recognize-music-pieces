import tensorflow as tf
import PIL
from PIL import Image
import cv2
import numpy as np
import os.path
import matplotlib.pyplot as plt
import numpy as np

import plotly as py
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from matplotlib import pyplot as plt
import pylab

# img = mpimg.imread('a.jpg')
# img2 = cv2.imread('b.jpg')
# cv2.imshow('FRAME',img2)
# plt.imshow(img)
# plt.show()

print 'Libraries have been loaded.'

img = cv2.imread('/Users/brekhunchenko/Desktop/IMG_5929.JPG', 0)


# # th2 = cv2.adaptiveThreshold(cv2.bitwise_not(img), 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 15, -2)
# th2  = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 99, 10)
#
# cv2.imwrite('/Users/brekhunchenko/Desktop/lines/lines_adaptive_threshold.jpg', th2)
#
# print 'here'
#
# horizontal = th2.copy()
# vertical = th2.copy()
#
# horizontal_kernel_size= horizontal.shape[1]/30
#
# horizontal = cv2.erode(horizontal, np.ones((2, 30), dtype=np.uint8))
# horizontal = cv2.dilate(horizontal, np.ones((2, 30), dtype=np.uint8))
#
# cv2.imwrite('/Users/brekhunchenko/Desktop/lines/lines_erode_dilate_horizontal.jpg', horizontal)
#
# vertical = cv2.erode(vertical, np.ones((20, 3), dtype=np.uint8))
# vertical = cv2.dilate(vertical, np.ones((20, 3), dtype=np.uint8))
#
# vertical = cv2.bitwise_not(vertical)
#
# cv2.imwrite('/Users/brekhunchenko/Desktop/lines/lines_erode_dilate_vertical.jpg', vertical)
#
# print 'here'

img = cv2.imread('/Users/brekhunchenko/Documents/Projects/Recognize_Music_Pieces/Files/multipleTargetImages/IMG_5956.JPG', 0)
img = cv2.medianBlur(img,11)
cimg = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)
img = cv2.adaptiveThreshold(img , 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 99, 10)

# circles = cv2.HoughCircles(img, cv2.HOUGH_GRADIENT, 1, 20,
#                             param1=50, param2=20, minRadius=1, maxRadius=100)

# circles = np.uint16(np.around(circles))
# for i in circles[0,:]:
    # draw the outer circle
    # cv2.circle(cimg,(i[0],i[1]),i[2],(0,255,0),2)
    # # draw the center of the circle
    # cv2.circle(cimg,(i[0],i[1]),2,(0,0,255),3)

cv2.imwrite('/Users/brekhunchenko/Desktop/lines/binary.jpg', img)

params = cv2.SimpleBlobDetector_Params()

# Change thresholds
params.minThreshold = 10
params.maxThreshold = 200

# Filter by Area.
params.filterByArea = True
params.minArea = 12
# params.maxArea = 30

#
# # Filter by Circularity
# params.filterByCircularity = True
# params.minCircularity = 0.1
#
# # Filter by Convexity
# params.filterByConvexity = True
# params.minConvexity = 0.5
#
# # Filter by Inertia
# params.filterByInertia = True
# params.minInertiaRatio = 0.01

# Create a detector with the parameters
ver = (cv2.__version__).split('.')
if int(ver[0]) < 3:
    detector = cv2.SimpleBlobDetector(params)
else:
    detector = cv2.SimpleBlobDetector_create(params)

# Detect blobs.
keypoints = detector.detect(img)

# Draw detected blobs as red circles.
# cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures
# the size of the circle corresponds to the size of blob

im_with_keypoints = cv2.drawKeypoints(cimg, keypoints, np.array([]), (0, 0, 255),
                                      cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

cv2.imwrite('/Users/brekhunchenko/Desktop/lines/blobs.jpg', im_with_keypoints)

print 'here'