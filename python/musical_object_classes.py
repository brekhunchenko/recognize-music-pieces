class MusicalObject:
    def __init__(self, name, point=None, pitch=None, midiCode = 0, staffData = None, topClosest = 0, topClosestGlobal = 0):
        self.name = name
        self.point = point
        self.pitch = pitch
        self.midiCode = midiCode
        self.staffData = staffData
        self.topClosest = topClosest
        self.topClosestGlobal = topClosestGlobal

class Staff:
    def __init__(self, lineSpacing=0, lineThickness=0, tops=[], lines =[], timeSignatures=[], keySignatures=[], staffStartX = 0, staffEndX = 0):
        self.lineSpacing = lineSpacing
        self.lineThickness = lineThickness
        self.tops = tops
        self.keySignatures = keySignatures
        self.timeSignatures = timeSignatures
        self.trebles = []
        self.basses = []
        self.lines = lines
        self.staffStartX = staffStartX
        self.staffEndX = staffEndX

