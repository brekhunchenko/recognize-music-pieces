import numpy as np
from scipy.signal import fftconvolve
import cv2
import musical_object_classes
import math
from scipy import stats
import matplotlib.pyplot as plt
import pylab
from scipy import signal
import matplotlib.pyplot as plt
import numpy as np
import plotly.plotly as py
import correct_midi_codes
import sys
import staff_data
from neural_network import train_cnn, detect_note_heads_positions, is_note_image, pixel_vec

def generateStaffTemplates():
    img = cv2.imread("/Users/brekhunchenko/Downloads/omr-master/Resources/binary_digital_strauss_blue_danube.jpg", 0)

def compareTemplate(template, img):
    result = cv2.matchTemplate(cv2.resize(img, (template.shape[1], template.shape[0])), template, cv2.TM_CCOEFF_NORMED)
    return result

def removeDuplicateMatches(threshold, points):
    result = []
    while (points):
        currentPoints = []
        nextPoints = []
        referencePoint = points[0]
        currentPoints.append(referencePoint)
        if (len(points) > 1):
            for i in range(1, len(points)):
                comparePoint = points[i]
                if (dist(referencePoint, comparePoint) < threshold):
                    currentPoints.append(comparePoint)
                else:
                    nextPoints.append(comparePoint)
        result.append(average(currentPoints))
        points = nextPoints
    return result

def dist(a, b):
    return math.sqrt((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2)

def average(points):
    sumX = 0
    sumY = 0
    for point in points:
        sumX += point[0]
        sumY += point[1]
    n = len(points)
    return (sumX / n, sumY / n)

def closestTopStaffForNote(point, topstaffs):
    minDistance = 10000
    pointY = point[1]
    for staffTop in staffData.tops:
        if abs(staffTop - pointY) < minDistance:
            minDistance = abs(staffTop - pointY)
            closestStaffTop = staffTop

    return closestStaffTop

def recognizeMusicNotes(img, staffData):
    img = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 99, 10)
    cv2.imwrite('/Users/brekhunchenko/Desktop/recognize_pipline/binary.png', img )

    templatePath = '../templates/'

    templates = (
        # ('note head', 'template_0.png', 20.0),
        # ('note head', 'template_1.png', 20.0),
        # ('note head', 'template_3.png', 20.0),
        ('note head', 'template_4.png', 20.0),
    )

    methods = [cv2.TM_CCOEFF, cv2.TM_CCOEFF_NORMED, cv2.TM_CCORR, cv2.TM_CCORR_NORMED, cv2.TM_SQDIFF,
               cv2.TM_SQDIFF_NORMED]

    # Each dictionary value is a list of MusicalObjects identified as an object of the type indicated by the key
    objects = {
        'treble clef': [],
        'bass clef': [],
        'crotchet rest': [],
        'time signature 8': [],
        'time signature 6': [],
        'time signature 4': [],
        'time signature 3': [],
        'quaver rest': [],
        'semibreve rest': [],
        'sharp': [],
        'natural': [],
        'flat': [],
        'note head': [],
        'minim note head': [],
        'key signature': [],
        'bar line': []
    }

    matchHighlightImage = img.copy()
    matchHighlightImage = cv2.cvtColor(matchHighlightImage, cv2.COLOR_GRAY2RGB)

    threshold = 0.9
    # for method in methods:
    for template in templates:
        templateImg = cv2.imread(templatePath + template[1], 0)
        height = len(templateImg)
        width = len(templateImg[1])
        templateImgScaled = cv2.resize(templateImg, None, fx=staffData.lineSpacing / template[2],
                                       fy=staffData.lineSpacing / template[2])
        result = cv2.matchTemplate(img, templateImgScaled, methods[1])

        musicalObjectPoints = []

        locations = np.where(result >= threshold)
        for point in zip(*locations[::-1]):
            cv2.rectangle(img, point, (point[0] + width, point[1] + height), 255, -1)
            musicalObjectPoints.append(point)
        musicalObjectPoints = removeDuplicateMatches(40, musicalObjectPoints)

        midiNotes = []
        for point in musicalObjectPoints:
            cv2.rectangle(matchHighlightImage, point, (point[0] + width, point[1] + height), (0, 0, 255), 2)

        print 'here'

    cv2.imwrite('/Users/brekhunchenko/Desktop/recognize_pipline/template_match_test.png', matchHighlightImage)
    return objects

def recognize_midi_codes_for_note_heads_positions(img, staffData, note_head_positions):

    matchHighlightImage = cv2.cvtColor(img.copy(), cv2.COLOR_GRAY2BGR)

    sortedMusicalObjectPoints = []
    for topStaff in staffData.tops:
        staffNotes = []
        for point in note_head_positions:
            if topStaff == closestTopStaffForNote(point, staffData.tops):
                staffNotes.append(point)

        staffNotes = sorted(staffNotes, key=lambda x: x[0])
        sortedMusicalObjectPoints.extend(staffNotes)

    musicalObjectPoints = sortedMusicalObjectPoints

    objects = []
    height, width = img.shape
    for point in musicalObjectPoints:
        closestStaffTop = closestTopStaffForNote(point, staffData.tops)
        staffCenter = closestStaffTop + staffData.lineThickness * 2 + staffData.lineSpacing * 2 + staffData.lineThickness / 2
        midiPitch = float(staffCenter - point[1]) / (staffData.lineThickness + staffData.lineSpacing)

        cv2.line(matchHighlightImage, (0, staffCenter), (matchHighlightImage.shape[0] - 1, staffCenter), (0, 0, 255), 3)

        if abs(midiPitch) < 4:
            cv2.rectangle(matchHighlightImage, point, (point[0] + width, point[1] + height), (0, 255, 0), 3)

            midiCode = 0
            if len(objects) > 0:
                previosNote = objects[-1]

                if np.allclose(previosNote.midiPitch, midiPitch, atol=0.1):
                    midiCode = 0
                else:
                    midiCode = -1 if previosNote.midiPitch > midiPitch else 1

            cv2.putText(matchHighlightImage, str(midiCode), (point[0] + width, point[1] + height),
                        cv2.FONT_HERSHEY_COMPLEX,
                        1.2, (0, 255, 0))

            musicalObj = musical_object_classes.MusicalObject('note head', point, (width, height), pitch=None,
                                                              midiPitch=midiPitch, midiCode=midiCode)
            objects.append(musicalObj)

    cv2.imwrite('/Users/brekhunchenko/Desktop/recognize_pipline/template_match_test.png', matchHighlightImage)
    return objects

train_cnn()
