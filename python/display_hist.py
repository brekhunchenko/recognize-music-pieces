import random
import numpy
import matplotlib.pyplot as plt
# import plotly as py
import numpy as np
import cv2
from skimage.measure import structural_similarity as ssim

def mse(imageA, imageB):
    err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
    err /= float(imageA.shape[0] * imageA.shape[1])
    return err


# def compare_images(imageA, imageB, title):
#     # compute the mean squared error and structural similarity
#     # index for the images
#     m = mse(imageA, imageB)
#     # s = ssim(imageA, imageB)
#
#     # setup the figure
#     fig = plt.figure(title)
#     plt.suptitle("MSE: %.2f, SSIM: %.2f" % (m, s))
#
#     # show first image
#     ax = fig.add_subplot(1, 2, 1)
#     plt.imshow(imageA, cmap=plt.cm.gray)
#     plt.axis("off")
#
#     # show the second image
#     ax = fig.add_subplot(1, 2, 2)
#     plt.imshow(imageB, cmap=plt.cm.gray)
#     plt.axis("off")
#
#     # show the images
#     plt.show()


# hist1 = compute_histogram(img_0)
# hist2 = compute_histogram(img_1)
# sc = mse(img_0, img_1)


methods = [cv2.TM_CCOEFF, cv2.TM_CCOEFF_NORMED, cv2.TM_CCORR, cv2.TM_CCORR_NORMED, cv2.TM_SQDIFF,
           cv2.TM_SQDIFF_NORMED]

img_0 = cv2.imread("/Users/brekhunchenko/Desktop/lines/binary_crop_0.png", 0)
img_1 = cv2.imread("/Users/brekhunchenko/Desktop/lines/binary_crop_1.png", 0)
img_2 = cv2.imread("/Users/brekhunchenko/Desktop/lines/binary_crop_2.png", 0)

a = ssim(img_0, img_1)

for method in methods:
    result = cv2.matchTemplate(img_0, img_1, method)
    print result

print ' '
for method in methods:
    result = cv2.matchTemplate(img_0, img_2, method)
    print result

print 'here'

# histogram=plt.figure()
#
# h1 = np.array([1.9565217391304348,
#  1.9565217391304348,
#  1.5217391304347827,
#  1.5217391304347827,
#  1.5217391304347827,
#  1.5217391304347827,
#  1.0869565217391304,
#  1.0869565217391304,
#  0.6521739130434783,
#  0.6521739130434783,
#  0.6521739130434783,
#  -0.21739130434782608,
#  -1.0869565217391304,
#  -1.0869565217391304,
#  -1.0869565217391304,
#  -1.9565217391304348,
#  2.391304347826087,
#  2.391304347826087,
#  1.5217391304347827,
#  1.5217391304347827,
#  1.5217391304347827,
#  1.5217391304347827,
#  1.5217391304347827,
#  0.6521739130434783,
#  0.6521739130434783,
#  0.6521739130434783,
#  0.6521739130434783,
#  -1.0869565217391304,
#  -1.0869565217391304,
#  -1.9565217391304348,
#  2.4347826086956523,
#  2.4347826086956523,
#  1.0869565217391304,
#  1.9565217391304348,
#  1.565217391304348,
#  1.5217391304347827,
#  1.5217391304347827,
#  0.6521739130434783,
#  0.6521739130434783,
#  0.6521739130434783,
#  -0.21739130434782608,
#  -1.0434782608695652,
#  -1.0434782608695652,
#  -1.0869565217391304,
#  -1.0869565217391304,
#  -1.9565217391304348,
#  -1.9565217391304348,
#  2.391304347826087,
#  2.391304347826087,
#  2.391304347826087,
#  2.391304347826087,
#  1.5217391304347827,
#  1.5217391304347827,
#  1.5217391304347827,
#  1.5217391304347827,
#  0.6521739130434783,
#  0.6521739130434783,
#  0.6521739130434783,
#  -0.21739130434782608,
#  -1.0869565217391304,
#  -1.0869565217391304,
#  3.260869565217391,
#  2.391304347826087,
#  2.391304347826087])
#
# h2 = np.array([2.6551724137931036,
#  -0.034482758620689655,
#  -0.13793103448275862,
#  -0.1724137931034483,
#  -0.8620689655172413,
#  -1.0,
#  -1.0,
#  -1.7241379310344827,
#  -1.793103448275862,
#  -0.034482758620689655,
#  -0.06896551724137931,
#  1.5862068965517242,
#  0.896551724137931,
#  0.8620689655172413,
#  0.7586206896551724,
#  0.7241379310344828,
#  0.6896551724137931,
#  1.5862068965517242,
#  0.7586206896551724,
#  0.7931034482758621,
#  0.7241379310344828,
#  0.7241379310344828,
#  -0.06896551724137931,
#  -0.896551724137931,
#  -1.0,
#  -1.0344827586206897,
#  -1.793103448275862,
#  -1.793103448275862,
#  2.0,
#  1.8620689655172413,
#  1.206896551724138,
#  1.4137931034482758,
#  0.10344827586206896,
#  -0.1724137931034483,
#  -0.20689655172413793,
#  -0.9310344827586207,
#  -1.896551724137931,
#  5.9655172413793105,
#  3.3793103448275863,
#  3.3793103448275863,
#  3.2758620689655173,
#  2.413793103448276,
#  0.3103448275862069])
#
#
# h1 = h1/h1.max()
# h2 = h2/h2.max()
#
# ax1 = histogram.add_subplot(111)
#
# ax1.scatter(h1, xrange(0, h1.shape[0]), s=10, c='b', marker="s", label='first')
# ax1.scatter(h2, xrange(0, h2.shape[0]), s=10, c='r', marker="o", label='second')
# plt.legend(loc='upper left');
# plt.gcf()
#
# # bins = numpy.linspace(0, 5, 0.1)
#
# # plt.hist(h1, xrange(0, h1.shape[0]), alpha=0.5)
# # plt.hist(h2, bins, alpha=0.5)
# # plt.gcf()
#
# print 'here'
