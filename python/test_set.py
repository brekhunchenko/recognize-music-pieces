import cv2
import numpy as np
from scipy import ndimage
# from simmilarity_images import dist
import math
import staff_data

def dist(a, b):
    return math.sqrt((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2)

#test_image
#IMG_6216.JPG
note_heads_positions = [(1069.2, 827.8),(1188.4, 832.9),(1304.3, 862.1),(1449.4, 856.1),(1563.9, 872.1),(1621.9, 862.5),(1678.9, 863.9),(1736.4, 889.4),(1810.4, 889.9),(2065.5, 855.7),(2180.5, 857.0),(2295.0, 879.4),(2436.0, 867.1),(2549.7, 879.9),(2606.7, 891.3),(606.0, 1228.1),(740.7, 1231.7),(876.7, 1210.3),(1035.0, 1226.7),(1166.9, 1244.0),(1231.7, 1233.5),(1296.1, 1235.8),(1360.4, 1238.1),(1422.0, 1274.6),(1486.8, 1265.0),(1584.0, 1255.0),(1712.7, 1255.9),(1775.2, 1257.3),(1837.3, 1258.2),(1899.8, 1258.2),(1963.3, 1236.3),(2474.8, 1199.8),(2601.2, 1198.4),(2728.6, 1223.0),(471.0, 1537.0),(626.6, 1548.4),(704.2, 1537.9),(780.4, 1539.3),(857.5, 1564.9),(936.0, 1566.2),(1262.7, 1536.1),(1410.6, 1537.5),(1554.4, 1564.4),(1726.4, 1553.4),(1870.6, 1566.2),(1943.6, 1579.0),(2423.7, 1602.7),(2567.0, 1601.8),(2710.8, 1577.6),(471.0, 1932.7),(631.6, 1944.1),(711.0, 1931.3),(790.0, 1930.9),(869.4, 1931.8),(948.8, 1969.2),(1026.8, 1956.4),(1139.5, 1945.9),(1292.9, 1945.0),(1369.1, 1945.5),(1445.3, 1945.5),(1520.6, 1957.3),(1596.3, 1969.2),(2085.1, 1886.6),(2261.7, 1874.3),(2409.6, 1884.8),(2559.3, 1898.5),(2709.4, 1872.0),(469.1, 2243.9),(817.3, 2241.2),(977.5, 2227.9),(1103.9, 2238.0),(1230.8, 2248.9),(1357.7, 2223.4),(1748.3, 2233.0),(1899.8, 2219.7),(2021.2, 2230.2),(2145.3, 2241.6),(2266.7, 2217.4),(2416.0, 2205.6),(2741.8, 2264.0),(466.9, 2633.6),(604.2, 2630.5),(672.7, 2615.8),(741.1, 2615.8),(808.2, 2601.7),(876.2, 2588.9),(1400.6, 2587.6),(1530.2, 2585.3),(1658.0, 2607.2),(1814.9, 2592.6),(1941.4, 2605.4),(2007.5, 2590.7),(2071.0, 2593.0),(2136.7, 2616.3),(2212.0, 2614.9),(2488.1, 2576.1),(2615.4, 2574.8),(2743.2, 2598.5),(464.1, 2987.3),(601.9, 2998.3),(673.1, 3010.1),(1130.4, 3016.5),(1264.1, 3012.0),(1396.0, 2982.8),(1553.9, 2988.7),(1685.8, 2999.6),(1750.1, 2985.5),(1815.9, 2985.5),(1882.0, 2983.2),(1946.8, 3020.6),(2013.9, 3006.5),(2111.6, 2994.2),(2240.3, 2992.8),(2306.4, 2993.7),(2369.4, 2992.3),(2433.3, 3003.3),(2498.1, 3014.2),]
img_path = "/Users/brekhunchenko/Desktop/test_set/IMG_6216.JPG"

dir = "/Users/brekhunchenko/Desktop/test_set/"

img = cv2.imread(img_path, 0)

staffData = staff_data.getStaffData(img)
note_head_size = 11
scale = float(note_head_size)/staffData.lineSpacing

neural_net_img_size = 22
# note_img_size = neural_net_img_size/scale
note_img_size = 60
note_index_offset = 0

for i in range(0, len(note_heads_positions)):
    note_pos = note_heads_positions[i]
    start_x = int((int(note_pos[0]) - note_img_size/2))
    end_x = int((int(note_pos[0]) + note_img_size/2))
    start_y = int((int(note_pos[1]) - note_img_size/2))
    end_y = int((int(note_pos[1]) + note_img_size/2))
    sub_im = img[start_y:end_y, start_x:end_x]
    sub_im = cv2.resize(sub_im, (neural_net_img_size, neural_net_img_size))

    path = dir + "solid_note_head/" + ("%d.jpg" % (i + note_index_offset))
    cv2.imwrite(path, sub_im)

background_index_offset = 0
counter = 0

cimg = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
copyOverlay = cimg.copy()

for i in range(int(note_img_size), img.shape[1], 32):
    for j in range(int(note_img_size), img.shape[0], 32):
        start_x = int(i - note_img_size / 2)
        end_x = int(i + note_img_size / 2)
        start_y = int(j - note_img_size / 2)
        end_y = int(j + note_img_size / 2)

        note_nearly = False
        for k in range(0, len(note_heads_positions)):
            note = note_heads_positions[k]
            if dist((i, j), note) < float(note_img_size):
                note_nearly = True
                cv2.rectangle(copyOverlay, (int(note[0]), int(note[1])), (int(note[0]+1), int(note[1]+1)),
                              (0, 0, 255), -1)
                break

        if note_nearly == False:
            sub_im = img[start_y:end_y, start_x:end_x]
            path = dir + "background/" + ("%d.jpg" % (counter + background_index_offset))
            sub_im = cv2.resize(sub_im, (neural_net_img_size, neural_net_img_size))
            if ndimage.variance(sub_im) > 700:
                cv2.imwrite(path, sub_im)

                counter = counter + 1

                cv2.rectangle(copyOverlay, (start_x, start_y), (i + neural_net_img_size, j + neural_net_img_size), (0, 0, 255), -1)

cimg = cv2.addWeighted(copyOverlay, 0.3, cimg, 0.7, 0)

cv2.imwrite("/Users/brekhunchenko/Desktop/test_set/background.jpg", cimg)

print '%d background images and %d note heads have been saved.' % (counter, len(note_heads_positions))