import numpy as np
from scipy.signal import fftconvolve
import cv2
import musical_object_classes
import math
from scipy import stats
import matplotlib.pyplot as plt
import pylab
from scipy import signal
import matplotlib.pyplot as plt
import numpy as np
import plotly.plotly as py
import correct_midi_codes
import sys

def dist(a, b):
    return math.sqrt((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2)

def findEdgesOfStaff(h_projection):
    left = 0
    right = len(h_projection)
    a = h_projection > h_projection.mean() * 2

    for i in xrange(0, len(a)):
        if a[i] == True:
            left = i
            break

    for i in xrange(0, len(a)):
        if a[-i] == True:
            right = len(a) - i
            break

    return left, right

def verticalRunLengthMode(img, colour):
    runLengths = []
    width = len(img[0])
    height = len(img)
    for x in range(width * 1 / 4, width * 3 / 4, int(width*0.05)):
        inColour = False
        currentRun = 0
        for y in range(0, height):
            if (img[y, x] == colour):
                if (inColour):
                    currentRun = currentRun + 1
                else:
                    currentRun = 1
                    inColour = True
            else:
                if (inColour):
                    runLengths.append(currentRun)
                    inColour = False

    return int(stats.mode(runLengths)[0][0])


def isTopStaffLine(rho, rhoValues, gap, threshold):
    # for i in range(1, 5):
    #     member = False
    #     for j in range(0, threshold + 1):
    #         if (rho + i * gap + j) in rhoValues or (rho + i * gap - j) in rhoValues:
    #             member = True
    #     if not (member):
    #         return False
    # return True
    for k, rhoValue in enumerate(rhoValues):
        if rhoValue == rho:
            counter = 0
            for i in range(1, 5):
                if k + i < len(rhoValues):
                    neighbor_rho = rhoValues[k + i]
                    if np.allclose(neighbor_rho, rho + i*gap, atol=threshold):
                        counter += 1

            return (counter == 4)

    return False


def getStaffData(imgInput, meanThreshold = 2, debug=False):

    imgInput = cv2.adaptiveThreshold(imgInput, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 99, 10)
    imgBinary = 255 - imgInput
    # imgBinary = imgInput

    # cv2.imwrite('/Users/brekhunchenko/Desktop/recognize_pipline/binary.png', imgBinary)

    staffLineThickness = verticalRunLengthMode(imgBinary, 255)

    if debug:
        print("staffLineThickness: " + str(staffLineThickness))

    staffLineSpacing = verticalRunLengthMode(imgBinary, 0) + staffLineThickness
    # staffLineSpacing = 24
    if debug:
        print("staffLineSpacing: " + str(staffLineSpacing))

    # horizontal_kernel_size = imgBinary.shape[1] / 30
    # horizontal = cv2.erode(imgBinary, np.ones((2, horizontal_kernel_size), dtype=np.uint8))
    # horizontal = cv2.dilate(horizontal, np.ones((2, horizontal_kernel_size), dtype=np.uint8))
    # cv2.imwrite('/Users/brekhunchenko/Desktop/recognize_pipline/lines_erode_dilate_horizontal.jpg', horizontal)

    sortedRhoValues = []
    h_projection = np.array([x/255 for x in imgBinary.sum(axis=1)])
    mean = h_projection.mean()
    for i in range(0, len(h_projection)):
        pixel = h_projection[i]
        peak = True
        width = int(staffLineSpacing*1.3)
        for neighbor_index in range(-width / 2, width / 2):
            projection_index = i + neighbor_index
            if 0 < projection_index < len(h_projection) and projection_index > 0 and projection_index < imgInput.shape[1]:
                neighbor_value = h_projection[projection_index ]
                if neighbor_value > pixel:
                    peak = False
                    break

        if peak and pixel > mean*meanThreshold:
            have_neighbor_line = False
            for rho in sortedRhoValues:
                if np.allclose(rho, i, atol=staffLineSpacing/3):
                    have_neighbor_line = True

            if not have_neighbor_line:
                sortedRhoValues.append(i)

    if debug:
        print("Founded %s peaks." + str(len(sortedRhoValues)))

    staffTops = []
    for rho in sortedRhoValues:
        if isTopStaffLine(rho, sortedRhoValues, staffLineSpacing, staffLineThickness):
            staffTops.append(rho)
    if debug:
        print("staffTops: " + str(staffTops))

    imgStaffLines = imgInput.copy()
    imgStaffLines = cv2.cvtColor(imgStaffLines, cv2.COLOR_GRAY2RGB)
    for rho in staffTops:
        for i in range(0, 5):
            y = rho + i * staffLineSpacing
            cv2.line(imgStaffLines, (0, y), (min(imgStaffLines.shape[0], imgStaffLines.shape[1]) - 1, y), (0, 0, 255), 2)

    # cv2.imwrite("/Users/brekhunchenko/Desktop/recognize_pipline/recognize_pipline_staff_lines.png", imgStaffLines)

    currentStaff = musical_object_classes.Staff(staffLineSpacing, staffLineThickness, staffTops, lines=sortedRhoValues)
    return currentStaff

def testVerticalThreshold(img, x, y, threshold):
    upperY = y
    lowerY = y
    while (upperY >= 0):
        if (img[upperY - 1, x] == 0):
            upperY -= 1
        else:
            break
    while (lowerY <= len(img)):
        if (img[lowerY + 1, x] == 0):
            lowerY += 1
        else:
            break
    return (lowerY - upperY <= threshold), upperY, lowerY

def cropTopStaffBarForImage(img, staffData):
    staffBarImages = []

    for i in range(0, len(staffData.tops)):
        staffCenter = staffData.tops[i] + staffData.lineThickness * 2 + staffData.lineSpacing * 2 + staffData.lineThickness / 2
        staffImageOffset = (staffData.lineThickness + staffData.lineSpacing) * 6
        topStaffLinesImage = img[(staffCenter - staffImageOffset):(staffCenter + staffImageOffset), :]

        staffBarImagePath = '/Users/brekhunchenko/Desktop/recognize_pipline/staffBar_%d.png' % i
        cv2.imwrite(staffBarImagePath, topStaffLinesImage)

        staffBarImages.append(topStaffLinesImage)

    print 'Cropped %d staff bar images.' % len(staffBarImages)

    return staffBarImages


def getPossibleNoteHeadsPositionsForStaffBar(staffBarImage, staffData):
    cimg = cv2.cvtColor(staffBarImage, cv2.COLOR_GRAY2BGRA)

    img = cv2.adaptiveThreshold(staffBarImage, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 99, 13)
    inversed_top_staff = cv2.bitwise_not(img)
    h_projection = np.array([x for x in inversed_top_staff.sum(axis=0)])
    peaks = []
    for i in range(0, len(h_projection)):
        pixel = h_projection[i]
        peak = True
        width = int(staffData.lineThickness*2.0)
        for neighbor_index in range(-width / 2, width / 2):
            if 0 < i + neighbor_index < len(h_projection):
                neighbor_value = h_projection[i + neighbor_index]
                if neighbor_value > pixel:
                    peak = False
                    break

        if peak and pixel > h_projection.mean()*2.0:
            peaks.append(i)

    # for peak in peaks:
    #     offset = 11
    #     copyOverlay = cimg.copy()
    #     cv2.rectangle(copyOverlay, (peak - offset, 0), (peak + offset, cimg.shape[0]), (0, 0, 255, 0.1), -1)
    #     cimg = cv2.addWeighted(copyOverlay, 0.3, cimg, 0.7, 0)

    # cv2.imwrite("/Users/brekhunchenko/Desktop/recognize_pipline/note_regions.jpg", cimg)

    return peaks

def removeStaffLines(img, staff):
    width = len(img[0])

    kernel = np.ones((10, staff.lineThickness), np.uint8)
    img = cv2.dilate(img, kernel, iterations=1)

    for y in staff.tops:
        for i in range(-2, 7):
            for j in range(0, staff.lineThickness + 1):
                currentY = y + staff.lineSpacing * i + j
                cv2.line(img, (0, currentY), (width, currentY), 255)

    img = cv2.erode(img, kernel, iterations=1)

    kernel = np.ones((staff.lineThickness * 2, staff.lineThickness * 2), np.uint8)
    img = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)

    cv2.imwrite('/Users/brekhunchenko/Desktop/recognize_pipline/removal.png', img)

    return img

def removeStaffLinesSP(img, staff):
    staffLineSpacing = staff.lineSpacing
    staffLineThickness = staff.lineThickness
    # Threshold for black pixel height
    threshold = staffLineSpacing / 2
    width = len(img[1])

    for i in staff.tops:
        for y in range(0, 4 * staffLineSpacing + 1, staffLineSpacing):
            # For each staff line,
            startX = 0
            for x in range(width - 1, 0, -1):
                if (img[i + y, x] == 0):
                    startX = x
                    # cv2.rectangle(img,(0,0),(x,i+y),127)
                    break
            yRef = i + y
            for x in range(x, 0, -1):
                if (not (img[yRef, x] == 0)):
                    # Find closest black pixel vertically
                    for j in range(1, threshold / 2):
                        if (img[yRef + j, x] == 0):
                            yRef = yRef + j
                            break
                        if (img[yRef - j, x] == 0):
                            yRef = yRef - j
                            break
                verticalThresholdResult = testVerticalThreshold(img, x, yRef, staffLineThickness * 5 / 4)
                # if (verticalThresholdResult[0]):
                #	yRef = (verticalThresholdResult[1] + verticalThresholdResult[2]) / 2
                if (verticalThresholdResult[0]):
                    cv2.line(img, (x, verticalThresholdResult[1]), (x, verticalThresholdResult[2]), 255)

    # Fix broken objects by erosion followed by dilation
    kernel = np.ones((staff.lineThickness, staff.lineThickness), np.uint8)
    img = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)

    # Output image with staff lines removed
    # parsedFilePath = sys.argv[1].split('/')
    # imageName = parsedFilePath[-1].split('.')[0]
    cv2.imwrite('/Users/brekhunchenko/Desktop/recognize_pipline/removal_1.png', img)

    return img

def getMidiCodes(image_name, staffData, music_objects, images_folder = "/Users/brekhunchenko/Desktop/music_sheets/training_set/",):
    img = cv2.imread(images_folder + image_name, 0)
    img = cv2.resize(img, (1512, 2016))

    midiCodes = []

    prevNoteHeadPitch = None

    cimg = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)

    for i, music_object in enumerate(music_objects):
        x, y = music_object.point

        midiCode = 0
        size = staffData.lineSpacing * 13
        subImageStaffData = getStaffDataForNoteHeadPos(img, staffData, music_object.point, size)

        if len(subImageStaffData.tops) > 0:

            center = music_object.topClosest + staffData.lineSpacing*2.0 + staffData.lineThickness/2.0
            notePitch = y - center

            if prevNoteHeadPitch != None:
                if np.allclose(prevNoteHeadPitch, notePitch, atol=staffData.lineThickness):
                    midiCode = 1
                elif prevNoteHeadPitch < notePitch:
                    midiCode = 0
                elif prevNoteHeadPitch > notePitch:
                    midiCode = 2

            midiCodes.append(midiCode)
            prevNoteHeadPitch = notePitch

            # cv2.rectangle(cimg, (int(x - staffData.lineSpacing), int(y - staffData.lineSpacing)),
            #               (int(x + staffData.lineSpacing * 1.5), int(y + staffData.lineSpacing * 1.5)), (0, 255, 0), 2)

            cv2.line(cimg, (x, y), (x, music_object.topClosest), (0, 255, 0), 2)
            cv2.putText(cimg, str(midiCode), (x - staffData.lineSpacing, music_object.topClosest),
                        cv2.FONT_HERSHEY_COMPLEX,
                        1.5, (0, 0, 255))

        else:
            print 'No staff line tops on the image'
            cv2.rectangle(cimg, (int(x - staffData.lineSpacing), int(y - staffData.lineSpacing)),
                          (int(x + staffData.lineSpacing * 1.5), int(y + staffData.lineSpacing * 1.5)), (0, 255, 0), 2)

    cv2.imwrite(("/Users/brekhunchenko/Documents/Projects/Recognize_Music_Pieces/Music_sheets_recognition_pipline/" + image_name + "/5_midi_codes_img.png"), cimg)

    midi_codes_str = ""
    for code in midiCodes:
        midi_codes_str += str(code)

    return midi_codes_str

def getStaffDataForNoteHeadPos(img, imgStaffData, noteHeadPos, size):
    start_x = (noteHeadPos[1] - size / 2) if (noteHeadPos[1] - size / 2) > 0 else 0
    end_x = (noteHeadPos[1] + size / 2) if (noteHeadPos[1] + size / 2) < img.shape[0] else img.shape[0]
    start_y = (noteHeadPos[0] - size / 2) if (noteHeadPos[0] - size / 2) > 0 else 0
    end_y = (noteHeadPos[0] + size / 2) if (noteHeadPos[0] + size / 2) < img.shape[1] else img.shape[1]
    subImg = img[start_x:end_x, start_y:end_y]
    subImageStaffData = getStaffData(subImg, meanThreshold=4, debug=False)

    if len(subImageStaffData.tops) == 0:
        angles_array = [-1, 1, -2, 2, -3, 3]
        for angle in angles_array:
            rotated_img = rotate(img, angle, center=noteHeadPos)
            subImg = rotated_img[start_x:end_x, start_y:end_y]
            subImageStaffData = getStaffData(subImg, debug=False)
            if len(subImageStaffData.tops) > 0:
                break

    return subImageStaffData

def rotate(image, angle, center = None, scale = 1.0):
    (h, w) = image.shape[:2]

    if center is None:
        center = np.array((w / 2, h / 2), dtype='uint32')

    M = cv2.getRotationMatrix2D(tuple(center.tolist()), angle, scale)
    rotated = cv2.warpAffine(image, M, (w, h))

    return rotated
