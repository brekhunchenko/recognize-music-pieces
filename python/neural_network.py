
"""
Convolutional Neural Network
Including training and testing functions using TensorFlow
"""


import tensorflow as tf
import os
import cv2
import numpy as np
import math

symbol_label_list = ["background", "solid_note_head"]
# symbol_label_list = ["bar_line", "treble_clef", "bass_clef", "alto_clef", "flat", "sharp", "natural", "solid_note_head", "open_note_head", "whole_note_head", "background"]
uni_size = (64, 64)
uni_image_pixels = uni_size[0]*uni_size[1]
# dir = "../training_data/" + label_name + "/"

def pixel_vec(img):
    u_img = unify_img_size(img)
    feature = np.squeeze(u_img.reshape(-1, u_img.size)/256.0) # using pixel features
    return feature

def unify_img_size(img):
    return cv2.resize(img, uni_size, 0, 0, cv2.INTER_LINEAR)

def weight_variable(shape, name=None):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial, name=name)

def bias_variable(shape, name=None):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial, name=name)

def conv2d(x, W, name=None):
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME', name=name)

def max_pool_2x2(x, name=None):
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                        strides=[1, 2, 2, 1], padding='SAME', name=name)

def max_pool_4x4(x, name=None):
    return tf.nn.max_pool(x, ksize=[1, 4, 4, 1],
                        strides=[1, 4, 4, 1], padding='SAME', name=name)

def get_test_data_batch(i, batch_size):
    cnt = 0
    n_labels = len(symbol_label_list)
    imgs = np.empty((0, uni_image_pixels), dtype = 'float32')
    labels = np.zeros((batch_size, n_labels), dtype='float32')
    circ = 0

    while cnt < batch_size:
        cur_label_index = circ % n_labels
        circ += 1

        label_name = symbol_label_list[cur_label_index]
        dir = "/Users/brekhunchenko/Desktop/test_set/" + label_name + "/"

        if not os.path.exists(dir):
            continue

        file_cnt = len([f for f in os.listdir(dir) if f.endswith('.jpg') and os.path.isfile(os.path.join(dir, f))])
        if file_cnt == 0:
            continue

        labels[cnt, cur_label_index] = 1
        picked_index = np.random.randint(file_cnt)
        img = cv2.imread(dir + str(picked_index) + ".jpg", 0)
        imgs = np.append(imgs, np.array([pixel_vec(img)]), axis = 0)
        cnt += 1


    batch = (imgs, labels)
    return batch

def get_data_batch(i, batch_size):
    cnt = 0
    n_labels = len(symbol_label_list)
    imgs = np.empty((0, uni_image_pixels), dtype = 'float32')
    labels = np.zeros((batch_size, n_labels), dtype='float32')
    circ = 0

    while cnt < batch_size:
        cur_label_index = circ % n_labels
        circ += 1

        label_name = symbol_label_list[cur_label_index]
        dir = "/Users/brekhunchenko/Desktop/training_set/" + label_name + "/"
        # dir = "../training_data/" + label_name + "/"

        if not os.path.exists(dir):
            continue

        file_cnt = len([f for f in os.listdir(dir) if f.endswith('.jpg') and os.path.isfile(os.path.join(dir, f))])
        if file_cnt == 0:
            continue

        labels[cnt, cur_label_index] = 1
        picked_index = i % file_cnt
        img = cv2.imread(dir + str(picked_index) + ".jpg", 0)
        imgs = np.append(imgs, np.array([pixel_vec(img)]), axis = 0)
        cnt += 1


    batch = (imgs, labels)
    return batch


def dist(a, b):
    return math.sqrt((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2)


def average(points):
    sumX = 0
    sumY = 0
    for point in points:
        sumX += point[0]
        sumY += point[1]
    n = len(points)
    return sumX / n, sumY / n


def removeDuplicateMatches(threshold, points):
    result = []
    while points:
        currentPoints = []
        nextPoints = []
        referencePoint = points[0]
        currentPoints.append(referencePoint)
        if len(points) > 1:
            for i in range(1, len(points)):
                comparePoint = points[i]
                if (dist(referencePoint, comparePoint) < threshold):
                    currentPoints.append(comparePoint)
                else:
                    nextPoints.append(comparePoint)
        result.append(average(currentPoints))
        points = nextPoints
    return result

def train_cnn():
    n_labels = len(symbol_label_list)
    x = tf.placeholder(tf.float32, shape=(None, uni_image_pixels), name='x')
    y_ = tf.placeholder(tf.float32, shape=(None, n_labels), name='y_')

    sess = tf.InteractiveSession()
    x_image = tf.reshape(x, [-1, uni_size[0], uni_size[1], 1], name='x_image')

    ##first convolutional layer
    W_conv1 = weight_variable([5, 5, 1, 32], name='W_conv1')
    b_conv1 = bias_variable([32], name='b_conv1')
    h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1, name='h_conv1')
    h_pool1 = max_pool_4x4(h_conv1, name='h_pool1')

    ##second convolutional layer
    W_conv2 = weight_variable([5, 5, 32, 64], name='W_conv2')
    b_conv2 = bias_variable([64], name='b_conv2')
    h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2, name='conv2d_h_pool1_W_conv2') + b_conv2, name='h_conv2')
    h_pool2 = max_pool_4x4(h_conv2, name='h_pool2')

    ##fully connected layer
    W_fc1 = weight_variable([4 * 4 * 64, 1024], name='W_fc1')
    b_fc1 = bias_variable([1024], name='b_fc1')
    h_pool2_flat = tf.reshape(h_pool2, [-1, 4 * 4 * 64], name='h_pool2_flat')
    h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1, name='h_fc1')

    #Readout layer
    W_fc3 = weight_variable([1024, n_labels], name='W_fc3')
    b_fc3 = bias_variable([n_labels], name='b_fc3')
    y_conv = tf.nn.softmax(tf.matmul(h_fc1, W_fc3, name='matmul_h_fc1_W_fc3') + b_fc3, name='softmax')

    cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y_conv), reduction_indices=[1]))
    train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)
    correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    find_y_conv = tf.argmax(y_conv,1)
    saver = tf.train.Saver()
    sess.run(tf.global_variables_initializer())

    batch_size = 50
    iter = 10000
    for i in range(iter):
        batch = get_data_batch(i, batch_size)

        if i % 50 == 0 and i != 0:
            test_batch = get_test_data_batch(i, 1000)

            train_accuracy = accuracy.eval(feed_dict={
                x: test_batch[0], y_: test_batch[1]})

            print 'Training accuracy: %.2f' % train_accuracy

            # print find_y_conv.eval(feed_dict={
            #     x: test_batch[0], y_: test_batch[1]})

            # print("Step %d, training accuracy %g" % (i, train_accuracy))

            # print y_conv.eval(feed_dict={x: test_batch[0]})


        train_step.run(feed_dict={x: batch[0], y_: batch[1]})

        if i % 100 == 0 and i != 0:
            saver.save(sess, '../models/model')
            tf.train.write_graph(sess.graph_def, "../models/", "graph.pb", False)

    print 'Neural network training finished.'

def detect_note_heads_positions(im, threshold = 0.99):
    n_labels = len(symbol_label_list)
    x = tf.placeholder(tf.float32, shape=(None, uni_image_pixels), name='x')

    sess = tf.InteractiveSession()
    x_image = tf.reshape(x, [-1, uni_size[0], uni_size[1], 1])

    ##first convolutional layer
    W_conv1 = weight_variable([5, 5, 1, 32], name='W_conv1')
    b_conv1 = bias_variable([32], name='b_conv1')
    h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)
    h_pool1 = max_pool_4x4(h_conv1)

    ##second convolutional layer
    W_conv2 = weight_variable([5, 5, 32, 64], name='W_conv2')
    b_conv2 = bias_variable([64], name='b_conv2')
    h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
    h_pool2 = max_pool_4x4(h_conv2)

    ##fully connected layer
    W_fc1 = weight_variable([4 * 4 * 64, 1024], name='W_fc1')
    b_fc1 = bias_variable([1024], name='b_fc1')
    h_pool2_flat = tf.reshape(h_pool2, [-1, 4 * 4 * 64])
    h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

    ##Readout layer
    W_fc3 = weight_variable([1024, n_labels], name='W_fc3')
    b_fc3 = bias_variable([n_labels], name='b_fc3')
    y_conv = tf.nn.softmax(tf.matmul(h_fc1, W_fc3) + b_fc3, name='softmax')

    sess.run(tf.global_variables_initializer())
    saver = tf.train.Saver()
    saver.restore(sess, '../models/model')

    cimg = cv2.cvtColor(im, cv2.COLOR_GRAY2BGR)
    tot_rows, tot_cols = im.shape[:2]
    [rows, cols] = [22, 22]
    step_size = 2
    note_head_points = []
    for i in range(0, tot_rows - rows, step_size):
        for j in range(0, tot_cols - cols, step_size):
            sub_im = im[i:i+22, j:j+22]
            batch = np.array([pixel_vec(sub_im)])
            prediction = y_conv.eval(feed_dict={x: batch})
            probability = prediction[0][np.argmax(prediction)]
            if symbol_label_list[np.argmax(prediction)] == 'solid_note_head' and probability > threshold:
                note_head_points.append((i, j))

        if i % 200 == 0 and i != 0:
            print 'Founded %d note heads.', len(note_head_points)
            note_points = removeDuplicateMatches(15, note_head_points)
            drawNoteHeads(cimg, note_points, threshold = threshold)

    note_points = removeDuplicateMatches(15, note_head_points)
    drawNoteHeads(cimg, note_points, threshold = threshold)

    return note_points

def is_note_image(img, threshold = 0.8):
    predictions = y_conv.eval(feed_dict={x: img})
    notes = np.zeros((predictions.shape[0]), dtype=bool)
    for i in range(predictions.shape[0]):
        prediction = predictions[i]
        probability = prediction[np.argmax(prediction)]
        if symbol_label_list[np.argmax(prediction)] == 'solid_note_head' and probability > threshold:
            notes[i] = True
        else:
            notes[i] = False

    return notes


def drawNoteHeads(cimg, note_points, threshold):
    for note_head in note_points:
        i, j = note_head
        # prob = note_head[1]

        # if prob >= threshold:
        cv2.rectangle(cimg, (j, i), (j + 22, i + 22), (0, 0, 255), 2)

    cv2.imwrite("/Users/brekhunchenko/Desktop/recognize_pipline/note_heads.jpg", cimg)

x_train = get_test_data_batch(0, 100)
print 'catch'
# n_labels = len(symbol_label_list)
# x = tf.placeholder(tf.float32, shape=(None, uni_image_pixels), name='x')
#
# sess = tf.InteractiveSession()
# x_image = tf.reshape(x, [-1, uni_size[0], uni_size[1], 1])
#
# ##first convolutional layer
# W_conv1 = weight_variable([5, 5, 1, 32], name='W_conv1')
# b_conv1 = bias_variable([32], name='b_conv1')
# h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)
# h_pool1 = max_pool_4x4(h_conv1)
#
# ##second convolutional layer
# W_conv2 = weight_variable([5, 5, 32, 64], name='W_conv2')
# b_conv2 = bias_variable([64], name='b_conv2')
# h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
# h_pool2 = max_pool_4x4(h_conv2)
#
# ##fully connected layer
# W_fc1 = weight_variable([4 * 4 * 64, 1024], name='W_fc1')
# b_fc1 = bias_variable([1024], name='b_fc1')
# h_pool2_flat = tf.reshape(h_pool2, [-1, 4 * 4 * 64])
# h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)
#
# ##Readout layer
# W_fc3 = weight_variable([1024, n_labels], name='W_fc3')
# b_fc3 = bias_variable([n_labels], name='b_fc3')
# y_conv = tf.nn.softmax(tf.matmul(h_fc1, W_fc3) + b_fc3, name='softmax')
#
# sess.run(tf.global_variables_initializer())
# saver = tf.train.Saver()
# saver.restore(sess, '../models/model')
